//FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   QScanCommand.h
//
// PROJECT:     QScanItl
//
// Feature implementation of extracting command line
// 
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//		YH		Yukang Han
//
/////////////////////////////////////////////
//
// Copyright (c) 2018 Innovent Technology Limited.
// All rigths reserved.
// 
// This software is protected by national and international copyright and
// other laws. Unauthorised use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
// 
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Innovent Technology Limited.
// 
///////////////////////////////////////////////////////////////////////////////

#ifndef _QSCANCOMMAND_H_
#define _QSCANCOMMAND_H_

#include <list>

struct CommandResult
{
	char*					streamURL;
	char*					username;
	char*					password;
	unsigned short			desiredPortNum;
	class Authenticator*	ourAuthenticator;
};

struct QScanCommand
{

public:
	QScanCommand(class UsageEnvironment* env, const char* progName);
	~QScanCommand();

	bool VerifyCommandNum(int argc);

	void operator()(int argc, char** argv);

	void usage();
	void shutdown();

	bool isWindow();

	class UsageEnvironment*		m_env;
	const char*					m_progName;

	bool						m_bWindow;

	std::list<CommandResult>	m_kCommandResults;
};

#endif
