#ifndef _QSCANFOURCC_H_
#define _QSCANFOURCC_H_

extern "C"
{
#include <libavcodec/avcodec.h>
}

#include "QScanCommonDefine.h"

#ifdef WORDS_BIGENDIAN
#   define VLC_FOURCC( a, b, c, d ) \
        ( ((uint32_t)d) | ( ((uint32_t)c) << 8 ) \
           | ( ((uint32_t)b) << 16 ) | ( ((uint32_t)a) << 24 ) )
#   define VLC_TWOCC( a, b ) \
        ( (uint16_t)(b) | ( (uint16_t)(a) << 8 ) )

#else
#   define VLC_FOURCC( a, b, c, d ) \
        ( ((uint32_t)a) | ( ((uint32_t)b) << 8 ) \
           | ( ((uint32_t)c) << 16 ) | ( ((uint32_t)d) << 24 ) )
#   define VLC_TWOCC( a, b ) \
        ( (uint16_t)(a) | ( (uint16_t)(b) << 8 ) )

#endif

#define VLC_CODEC_MJPG            VLC_FOURCC('M','J','P','G')
#define VLC_CODEC_MP4V            VLC_FOURCC('m','p','4','v')
#define VLC_CODEC_VP8             VLC_FOURCC('V','P','8','0')
#define VLC_CODEC_THEORA          VLC_FOURCC('t','h','e','o')
#define VLC_CODEC_MPGV            VLC_FOURCC('m','p','g','v')
#define VLC_CODEC_H261            VLC_FOURCC('h','2','6','1')
#define VLC_CODEC_H263            VLC_FOURCC('h','2','6','3')
#define VLC_CODEC_H264            VLC_FOURCC('h','2','6','4')
#define VLC_CODEC_HEVC            VLC_FOURCC('h','e','v','c')

/** ES Categories */
enum es_format_category_e
{
	UNKNOWN_ES = 0x00,
	VIDEO_ES,
	AUDIO_ES,
	SPU_ES,
	NAV_ES,
};

static const struct
{
	unsigned int i_fourcc;
	unsigned i_codec;
	int i_cat;
} codecs_table[] =
{
	{ 0, AV_CODEC_ID_NONE, UNKNOWN_ES },
	{ VLC_CODEC_MPGV, AV_CODEC_ID_MPEG2VIDEO, VIDEO_ES }, /* prefer MPEG2 over MPEG1 */
	{ VLC_CODEC_H261, AV_CODEC_ID_H261, VIDEO_ES },
	{ VLC_CODEC_H263, AV_CODEC_ID_H263, VIDEO_ES },
	{ VLC_CODEC_H264, AV_CODEC_ID_H264, VIDEO_ES },
	{ VLC_CODEC_HEVC, AV_CODEC_ID_HEVC, VIDEO_ES }
};

static const size_t codecs_count = sizeof(codecs_table) / sizeof(codecs_table[0]);

static bool GetFfmpegCodec(vlc_fourcc_t i_fourcc, int *pi_cat, unsigned *pi_ffmpeg_codec, const char **ppsz_name)
{
	for (unsigned i = 0; i < codecs_count; i++)
	{
		if (codecs_table[i].i_fourcc == i_fourcc)
		{
			if (pi_cat) *pi_cat = codecs_table[i].i_cat;
			if (pi_ffmpeg_codec) *pi_ffmpeg_codec = codecs_table[i].i_codec;
			return true;
		}
	}
	return false;
}

#endif // !_QSCANFOURCC_H_
