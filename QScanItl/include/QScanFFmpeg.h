#ifndef _QSCANFFMPEG_H_
#define _QSCANFFMPEG_H_

#include "liveMedia.hh"

extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"

#define INBUF_SIZE 4096
}

class QScanFFmpeg
{

public:
	
	QScanFFmpeg();
	~QScanFFmpeg();

public:

	bool CreateCodec();
	bool DestroyCodec();
	void SetSprop(u_int8_t const* prop, unsigned size);

	bool CreateDisplay(class QScanWindowDisplay* pDisplay);
	bool DestroyDisplay();

	bool ReceiveBuffer(u_int8_t* fReceiveBuffer, unsigned frameSize);

	void PreUpdate(unsigned int nDelta){};
	void Update(unsigned int nDelta);
	void PostUpdate(unsigned int nDelta){};

	void PreRender(unsigned int nDelta){};
	void Render(unsigned int nDelta);
	void PostRender(unsigned int nDelta){};

private:

	bool SetupVideoDec(AVCodecID nCodecID);
	bool SetupCodecExtra(AVCodecID nCodecID);

private:

	bool send(AVPacket* packet);
	
	bool receive(AVFrame* frame);

	bool ConvertToRGB();

private:

	AVPacket						m_kPacket;
	AVFrame*						m_pFrame;
	AVCodec*						m_pCodec;
	AVCodecContext*					m_pCodecContext;
	AVCodecParserContext*			m_pCodecParser;
	AVFormatContext*				m_pFormatContext;

	bool							m_bHasReceived;
	class QScanWindowDisplay*		m_pDisplay;

	struct SwsContext*				sws_ctxRGB;
	AVFrame*						m_pFrameRGB;
	unsigned char*					m_pOutBufferRGB;
	unsigned int					m_nOutBufferSize;
};


#endif // !_QSCANFFMPEG_H_
