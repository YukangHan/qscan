#ifndef _QSCANWINDOW_H_
#define _QSCANWINDOW_H_

class QScanWindow
{

public:

	static const unsigned short DEFAULT_WIDTH = 1280;
	static const unsigned short DEFAULT_HEIGHT = 720;

	const unsigned short SUBDISPLAY_WIDTH = 640;
	const unsigned short SUBDISPLAY_HEIGHT = 360;

 	//const unsigned short SUBDISPLAY_WIDTH = 320;
 	//const unsigned short SUBDISPLAY_HEIGHT = 240;

	//const unsigned short SUBDISPLAY_WIDTH = 1280;
	//const unsigned short SUBDISPLAY_HEIGHT = 720;

public:

	QScanWindow(unsigned short width = DEFAULT_WIDTH, unsigned short height = DEFAULT_HEIGHT);
	~QScanWindow();

	unsigned short		GetWidth()		{ return m_nWidth; };
	unsigned short		GetHeight()		{ return m_nHeight; };

	
	class QScanWindowDisplay*	CreateDisplay(unsigned short nIndex);
	
	bool		OpenSDLWindow();
	
	bool		HandleWindowEvents();

	void		PreUpdate(unsigned int nDelta);

	void		Update(unsigned int nDelta);

	void		PostUpdate(unsigned int nDelta);

	void		PreRender(unsigned int nDelta);

	void		Render(unsigned int nDelta);

	void		PostRender(unsigned int nDelta);

private:

	unsigned short			m_nWidth;
	unsigned short			m_nHeight;
	
	struct SDL_Window*		screen;
	struct SDL_Renderer*	sdlRenderer;

public:
	class QScanWindowDisplay* m_ptestdisplay;

};

#endif // !_QSCANWINDOW_H_
