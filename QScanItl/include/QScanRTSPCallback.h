#ifndef _QSCANRTSPCALLBACK_H_
#define _QSCANRTSPCALLBACK_H_

#include <stdint.h>
#include "RTSPClient.hh"
#include "UsageEnvironment.hh"

// Forward function definitions:

// RTSP 'response handlers':
void continueAfterOPTIONS(RTSPClient* rtspClient, int resultCode, char* resultString);
void continueAfterDESCRIBE(RTSPClient* rtspClient, int resultCode, char* resultString);
void continueAfterSETUP(RTSPClient* rtspClient, int resultCode, char* resultString);
void continueAfterPLAY(RTSPClient* rtspClient, int resultCode, char* resultString);

// Other event handler functions:
void subsessionAfterPlaying(void* clientData); // called when a stream's subsession (e.g., audio or video substream) ends
void subsessionByeHandler(void* clientData); // called when a RTCP "BYE" is received for a subsession
void streamTimerHandler(void* clientData);
// called at the end of a stream's expected duration (if the stream has not already signaled its end using a RTCP "BYE")

// Used to iterate through each stream's 'subsessions', setting up each one:
void setupNextSubsession(RTSPClient* rtspClient);

// Used to shut down and close a stream (including its "RTSPClient" object):
void shutdownStream(RTSPClient* rtspClient, int exitCode = 1);

void usage(UsageEnvironment& env, char const* progName);


static unsigned char* parseH264ConfigStr(char const* configStr, unsigned int& configSize);
static size_t vlc_b64_decode_binary_to_buffer(uint8_t *p_dst, size_t i_dst_max, const char *psz_src);

#endif // !_QSCANRTSPCALLBACK_H_
