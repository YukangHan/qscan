#ifndef _QSCANCOMMONDEFINE_H_
#define _QSCANCOMMONDEFINE_H_

extern "C"
{
#include <libavcodec/avcodec.h>
}

/* LIBAVCODEC_VERSION_CHECK checks for the right version of libav and FFmpeg
* a is the major version
* b and c the minor and micro versions of libav
* d and e the minor and micro versions of FFmpeg */
#define LIBAVCODEC_VERSION_CHECK( a, b, c, d, e ) \
    ( (LIBAVCODEC_VERSION_MICRO <  100 && LIBAVCODEC_VERSION_INT >= AV_VERSION_INT( a, b, c ) ) || \
      (LIBAVCODEC_VERSION_MICRO >= 100 && LIBAVCODEC_VERSION_INT >= AV_VERSION_INT( a, d, e ) ) )

/* Helper for GCC version checks */
#ifdef __GNUC__
# define VLC_GCC_VERSION(maj,min) \
    ((__GNUC__ > (maj)) || (__GNUC__ == (maj) && __GNUC_MINOR__ >= (min)))
#else
# define VLC_GCC_VERSION(maj,min) (0)
#endif

/* Function attributes for compiler warnings */
#ifdef __GNUC__
# define VLC_DEPRECATED __attribute__((deprecated))

# if defined( _WIN32 ) && VLC_GCC_VERSION(4,4)
#  define VLC_FORMAT(x,y) __attribute__ ((format(gnu_printf,x,y)))
# else
#  define VLC_FORMAT(x,y) __attribute__ ((format(printf,x,y)))
# endif
# define VLC_FORMAT_ARG(x) __attribute__ ((format_arg(x)))

# define VLC_MALLOC __attribute__ ((malloc))
# define VLC_NORETURN __attribute__ ((noreturn))

# if VLC_GCC_VERSION(3,4)
#  define VLC_USED __attribute__ ((warn_unused_result))
# else
#  define VLC_USED
# endif

#else
# define VLC_DEPRECATED
# define VLC_FORMAT(x,y)
# define VLC_FORMAT_ARG(x)
# define VLC_MALLOC
# define VLC_NORETURN
# define VLC_USED
#endif

/** Byte swap (16 bits) */
VLC_USED
static inline uint16_t bswap16(uint16_t x)
{
	return (x << 8) | (x >> 8);
}

/** Byte swap (32 bits) */
VLC_USED
static inline uint32_t bswap32(uint32_t x)
{
#if VLC_GCC_VERSION(4,3) || defined(__clang__)
	return __builtin_bswap32(x);
#else
	return ((x & 0x000000FF) << 24)
		| ((x & 0x0000FF00) << 8)
		| ((x & 0x00FF0000) >> 8)
		| ((x & 0xFF000000) >> 24);
#endif
}

/** Byte swap (64 bits) */
VLC_USED
static inline uint64_t bswap64(uint64_t x)
{
#if VLC_GCC_VERSION(4,3) || defined(__clang__)
	return __builtin_bswap64(x);
#elif !defined (__cplusplus)
	return ((x & 0x00000000000000FF) << 56)
		| ((x & 0x000000000000FF00) << 40)
		| ((x & 0x0000000000FF0000) << 24)
		| ((x & 0x00000000FF000000) << 8)
		| ((x & 0x000000FF00000000) >> 8)
		| ((x & 0x0000FF0000000000) >> 24)
		| ((x & 0x00FF000000000000) >> 40)
		| ((x & 0xFF00000000000000) >> 56);
#else
	return ((x & 0x00000000000000FFULL) << 56)
		| ((x & 0x000000000000FF00ULL) << 40)
		| ((x & 0x0000000000FF0000ULL) << 24)
		| ((x & 0x00000000FF000000ULL) << 8)
		| ((x & 0x000000FF00000000ULL) >> 8)
		| ((x & 0x0000FF0000000000ULL) >> 24)
		| ((x & 0x00FF000000000000ULL) >> 40)
		| ((x & 0xFF00000000000000ULL) >> 56);
#endif
}

/* MSB (big endian)/LSB (little endian) conversions - network order is always
* MSB, and should be used for both network communications and files. */
#ifdef WORDS_BIGENDIAN
# define hton16(i) ((uint16_t)(i))
# define hton32(i) ((uint32_t)(i))
# define hton64(i) ((uint64_t)(i))
#else
# define hton16(i) bswap16(i)
# define hton32(i) bswap32(i)
# define hton64(i) bswap64(i)
#endif
#define ntoh16(i) hton16(i)
#define ntoh32(i) hton32(i)
#define ntoh64(i) hton64(i)

/** Reads 16 bits in network byte order */
VLC_USED
static inline uint16_t U16_AT(const void *p)
{
	uint16_t x;

	memcpy(&x, p, sizeof(x));
	return ntoh16(x);
}

/** Reads 32 bits in network byte order */
VLC_USED
static inline uint32_t U32_AT(const void *p)
{
	uint32_t x;

	memcpy(&x, p, sizeof(x));
	return ntoh32(x);
}

/** Reads 64 bits in network byte order */
VLC_USED
static inline uint64_t U64_AT(const void *p)
{
	uint64_t x;

	memcpy(&x, p, sizeof(x));
	return ntoh64(x);
}

#define GetWBE(p)  U16_AT(p)
#define GetDWBE(p) U32_AT(p)
#define GetQWBE(p) U64_AT(p)

/**
* The vlc_fourcc_t type.
*
* See http://www.webartz.com/fourcc/ for a very detailed list.
*/
typedef uint32_t vlc_fourcc_t;


/**
* video palette data
* \see video_format_t
* \see subs_format_t
*/
#define VIDEO_PALETTE_COLORS_MAX 256

struct video_palette_t
{
	int i_entries;      /**< to keep the compatibility with libavcodec's palette */
	uint8_t palette[VIDEO_PALETTE_COLORS_MAX][4];  /**< 4-byte RGBA/YUVA palette */
};

/**
* Picture orientation.
*/
typedef enum video_orientation_t
{
	ORIENT_TOP_LEFT = 0, /**< Top line represents top, left column left. */
	ORIENT_TOP_RIGHT, /**< Flipped horizontally */
	ORIENT_BOTTOM_LEFT, /**< Flipped vertically */
	ORIENT_BOTTOM_RIGHT, /**< Rotated 180 degrees */
	ORIENT_LEFT_TOP, /**< Transposed */
	ORIENT_LEFT_BOTTOM, /**< Rotated 90 degrees clockwise */
	ORIENT_RIGHT_TOP, /**< Rotated 90 degrees anti-clockwise */
	ORIENT_RIGHT_BOTTOM, /**< Anti-transposed */

	ORIENT_NORMAL = ORIENT_TOP_LEFT,
	ORIENT_TRANSPOSED = ORIENT_LEFT_TOP,
	ORIENT_ANTI_TRANSPOSED = ORIENT_RIGHT_BOTTOM,
	ORIENT_HFLIPPED = ORIENT_TOP_RIGHT,
	ORIENT_VFLIPPED = ORIENT_BOTTOM_LEFT,
	ORIENT_ROTATED_180 = ORIENT_BOTTOM_RIGHT,
	ORIENT_ROTATED_270 = ORIENT_LEFT_BOTTOM,
	ORIENT_ROTATED_90 = ORIENT_RIGHT_TOP,
} video_orientation_t;

/**
* video format description
*/
struct video_format_t
{
	vlc_fourcc_t i_chroma;                               /**< picture chroma */

	unsigned int i_width;                                 /**< picture width */
	unsigned int i_height;                               /**< picture height */
	unsigned int i_x_offset;               /**< start offset of visible area */
	unsigned int i_y_offset;               /**< start offset of visible area */
	unsigned int i_visible_width;                 /**< width of visible area */
	unsigned int i_visible_height;               /**< height of visible area */

	unsigned int i_bits_per_pixel;             /**< number of bits per pixel */

	unsigned int i_sar_num;                   /**< sample/pixel aspect ratio */
	unsigned int i_sar_den;

	unsigned int i_frame_rate;                     /**< frame rate numerator */
	unsigned int i_frame_rate_base;              /**< frame rate denominator */

	uint32_t i_rmask, i_gmask, i_bmask;          /**< color masks for RGB chroma */
	int i_rrshift, i_lrshift;
	int i_rgshift, i_lgshift;
	int i_rbshift, i_lbshift;
	video_palette_t *p_palette;              /**< video palette from demuxer */
	video_orientation_t orientation;                /**< picture orientation */
};

/**
* ES language definition
*/
typedef struct extra_languages_t
{
	char *psz_language;
	char *psz_description;
} extra_languages_t;

/**
* ES format definition
*/
#define ES_PRIORITY_NOT_SELECTABLE  -2
#define ES_PRIORITY_NOT_DEFAULTABLE -1
#define ES_PRIORITY_SELECTABLE_MIN   0
#define ES_PRIORITY_MIN ES_PRIORITY_NOT_SELECTABLE
struct es_format_t
{
	int             i_cat;              /**< ES category @see es_format_category_e */
	vlc_fourcc_t    i_codec;            /**< FOURCC value as used in vlc */
	vlc_fourcc_t    i_original_fourcc;  /**< original FOURCC from the container */

	int             i_id;       /**< es identifier, where means
								-1: let the core mark the right id
								>=0: valid id */
	int             i_group;    /**< group identifier, where means:
								-1 : standalone
								>= 0 then a "group" (program) is created
								for each value */
	int             i_priority; /**< priority, where means:
								-2 : mean not selectable by the users
								-1 : mean not selected by default even
								when no other stream
								>=0: priority */

	char            *psz_language;        /**< human readible language name */
	char            *psz_description;     /**< human readible description of language */
	int             i_extra_languages;    /**< length in bytes of extra language data pointer */
	extra_languages_t *p_extra_languages; /**< extra language data needed by some decoders */


	video_format_t video;     /**< description of video format */

	unsigned int   i_bitrate; /**< bitrate of this ES */
	int      i_profile;       /**< codec specific information (like real audio flavor, mpeg audio layer, h264 profile ...) */
	int      i_level;         /**< codec specific information: indicates maximum restrictions on the stream (resolution, bitrate, codec features ...) */

	bool     b_packetized;  /**< whether the data is packetized (ie. not truncated) */
	int     i_extra;        /**< length in bytes of extra data pointer */
	void    *p_extra;       /**< extra data needed by some decoders or muxers */

};



#endif // !_QSCANCOMMONDEFINE_H_

