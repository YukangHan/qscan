#ifndef _QSCANWINDOWDISPLAY_H_
#define _QSCANWINDOWDISPLAY_H_

class QScanWindowDisplay
{

public:

	QScanWindowDisplay();
	~QScanWindowDisplay();

	bool InitlizeDisplay(unsigned short nWindowWidth, unsigned short nWindowHeight, unsigned short nPosX, unsigned short nPosY, unsigned short nWidth, unsigned short nHeight, struct SDL_Renderer* sdlRenderer);

	void Update(unsigned int nDelta);
	
	void Render(unsigned int nDelta, struct AVFrame* frame);


public:

	unsigned short			m_nDisplayWidth;
	unsigned short			m_nDisplayHeight;
	unsigned short			m_nPositionX;
	unsigned short			m_nPositionY;

	unsigned short			m_nWindowWidth;
	unsigned short			m_nWindowHeight;

	unsigned char*			m_pOutBufferYUV;
	unsigned int			m_nOutBufferSize;
	struct AVFrame*			m_pFrameYUV;
	struct SDL_Texture*		m_sdlTexture;
	struct SDL_Renderer*	m_sdlRenderer;
	struct SwsContext*		sws_ctx;
};

#endif
