#ifndef _QSCANUSAGEENVIRONMENT_H_
#define _QSCANUSAGEENVIRONMENT_H_

#include "BasicUsageEnvironment.hh"

class QScanUsageEnvironment : public BasicUsageEnvironment
{
public:
	QScanUsageEnvironment(TaskScheduler& taskScheduler);

public:
	static QScanUsageEnvironment* createNew(TaskScheduler& taskScheduler);

};

//////////////////////////////////////////////////////////////////////////
class QScanTaskScheduler : public BasicTaskScheduler
{
public:
	QScanTaskScheduler(unsigned maxSchedulerGranularity);

public:
	static QScanTaskScheduler* createNew(unsigned maxSchedulerGranularity = 10000/*microseconds*/);

public:
	virtual void doEventLoop(char volatile* watchVariable);

};


#endif
