//FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   QScanApplication.h
//
// PROJECT:     QScanItl
//
// Application Layer for QScanItl
// 
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//		YH		Yukang Han
//
/////////////////////////////////////////////
//
// Copyright (c) 2018 Innovent Technology Limited.
// All rigths reserved.
// 
// This software is protected by national and international copyright and
// other laws. Unauthorised use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
// 
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Innovent Technology Limited.
// 
///////////////////////////////////////////////////////////////////////////////
#ifndef _QSCANAPPLICATION_H_
#define _QSCANAPPLICATION_H_

#include <list>


class QScanApplication
{

public:

	QScanApplication();
	~QScanApplication();

	static QScanApplication& get()
	{
		static QScanApplication instance;
		return instance;
	}

public:

	bool Initialize(int argc, char** argv);

	bool UnInitialize();

	bool Run();

	void PreUpdate(unsigned int nDelta);

	void Update(unsigned int nDelta);

	void PostUpdate(unsigned int nDelta);

	void PreRender(unsigned int nDelta);

	void Render(unsigned int nDelta);

	void PostRender(unsigned int nDelta);

	void AddRTSPClient(class RTSPClient* client);

	void RemoveRTSPClient(class RTSPClient* client);

	bool OpenURL();

	bool OpenWindow();

	void FeedHalconImage(unsigned int nWidth, unsigned int nHeight, unsigned char* pImagePixel, unsigned int nImagePixelSize);

	void NotifySuccessOrFailure();

private:

	bool InitializeEnvironment(const char* progName);

	bool UnInitializeEnvironment();

	bool InitializeCommand(int argc, char** argv);

	bool UnInitializeCommand();

	bool InitializeCodec();

	bool UnInitializeCodec();

	bool InitializeWindow();

	bool UnInitializeWindow();

	bool InitializeHalcon();

	bool UnInitializeHalcon();

	void SetupWindowDisplay();

	bool IsOverallSucceeded();

	

private:

	class QScanTaskScheduler*		m_pScheduler;

	const char*						m_progName;

	class QScanUsageEnvironment*	m_pEnvironment;

	class QScanWindow*				m_pQScanWindow;

	struct QScanCommand*			m_QScanCommand;

	std::list<class RTSPClient*>	m_ClientList;

	bool							m_bOverallSucceeded;


};

#endif
