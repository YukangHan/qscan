#ifndef _QSCANSTREAMCLIENTSTATE_H_
#define _QSCANSTREAMCLIENTSTATE_H_

#include "MediaSession.hh"
#include "UsageEnvironment.hh"
#include "QScanFFmpeg.h"


// Define a class to hold per-stream state that we maintain throughout each stream's lifetime:

class StreamClientState
{
public:
	StreamClientState();
	virtual ~StreamClientState();

public:
	MediaSubsessionIterator* iter;
	MediaSession* session;
	MediaSubsession* subsession;
	TaskToken streamTimerTask;
	double duration;
	QScanFFmpeg ffmpeg;
	bool bSuccess;
};

#endif // !_QSCANSTREAMCLIENTSTATE_H_