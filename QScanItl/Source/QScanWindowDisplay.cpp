//FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   QScanWindowDisplay.cpp
//
// PROJECT:     QScanItl
//
// Displaying screen of different camera sources
// 
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//		YH		Yukang Han
//
/////////////////////////////////////////////
//
// Copyright (c) 2018 Innovent Technology Limited.
// All rigths reserved.
// 
// This software is protected by national and international copyright and
// other laws. Unauthorised use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
// 
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Innovent Technology Limited.
// 
///////////////////////////////////////////////////////////////////////////////
#include "QScanWindowDisplay.h"
#include <SDL.h>

extern "C"
{
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
#include <libavutil/frame.h>
}

//////////////////////////////////////////////////////////////////////////
QScanWindowDisplay::QScanWindowDisplay()
{
	m_nWindowWidth = m_nWindowHeight = m_nDisplayWidth = m_nDisplayHeight = m_nPositionX = m_nPositionY = m_nOutBufferSize = 0;

	m_pOutBufferYUV = nullptr; m_pFrameYUV = nullptr; m_sdlTexture = nullptr; m_sdlRenderer = nullptr; sws_ctx = nullptr;
}
//////////////////////////////////////////////////////////////////////////
QScanWindowDisplay::~QScanWindowDisplay()
{

}
//////////////////////////////////////////////////////////////////////////
bool QScanWindowDisplay::InitlizeDisplay(unsigned short nWindowWidth, unsigned short nWindowHeight, unsigned short nPosX, unsigned short nPosY, unsigned short nWidth, unsigned short nHeight, struct SDL_Renderer* sdlRenderer)
{
	m_nWindowWidth = nWindowWidth;		m_nWindowHeight = nWindowHeight;

	m_nPositionX = nPosX;				m_nPositionY = nPosY;
	m_nDisplayWidth = nWidth;			m_nDisplayHeight = nHeight;

	m_sdlRenderer = sdlRenderer;

	m_sdlTexture = SDL_CreateTexture(m_sdlRenderer, SDL_PIXELFORMAT_IYUV, SDL_TEXTUREACCESS_STREAMING, m_nDisplayWidth, m_nDisplayHeight);

	if (!m_sdlTexture)
	{
		return false;
	}

	sws_ctx = sws_getContext(m_nWindowWidth, m_nWindowHeight, AV_PIX_FMT_YUV420P, m_nDisplayWidth, m_nDisplayHeight, AV_PIX_FMT_YUV420P, SWS_BICUBIC, NULL, NULL, NULL);

	m_pFrameYUV = av_frame_alloc();

	if (!m_pFrameYUV)
	{
		return false;
	}

	m_nOutBufferSize = av_image_get_buffer_size(AV_PIX_FMT_YUV420P, m_nDisplayWidth, m_nDisplayHeight, 1);
	
	m_pOutBufferYUV = (unsigned char*)av_malloc(m_nOutBufferSize);

	if (!m_pOutBufferYUV)
	{
		return false;
	}

	av_image_fill_arrays(m_pFrameYUV->data, m_pFrameYUV->linesize, m_pOutBufferYUV, AV_PIX_FMT_YUV420P, m_nDisplayWidth, m_nDisplayHeight, 1);

	return true;
}
//////////////////////////////////////////////////////////////////////////
void QScanWindowDisplay::Update(unsigned int nDelta)
{

}
//////////////////////////////////////////////////////////////////////////
void QScanWindowDisplay::Render(unsigned int nDelta, struct AVFrame* frame)
{
	sws_scale(sws_ctx, frame->data, frame->linesize, 0, m_nWindowHeight, m_pFrameYUV->data, m_pFrameYUV->linesize);

	SDL_Rect sdlRect;
	sdlRect.x = m_nPositionX;
	sdlRect.y = m_nPositionY;
	sdlRect.w = m_nDisplayWidth;
	sdlRect.h = m_nDisplayHeight;

	SDL_UpdateYUVTexture(m_sdlTexture, NULL,
		m_pFrameYUV->data[0], m_pFrameYUV->linesize[0],
		m_pFrameYUV->data[1], m_pFrameYUV->linesize[1],
		m_pFrameYUV->data[2], m_pFrameYUV->linesize[2]);

	SDL_RenderCopy(m_sdlRenderer, m_sdlTexture, NULL, &sdlRect);
}