//FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   QScanCommand.cpp
//
// PROJECT:     QScanItl
//
// Feature implementation of extracting command line
// 
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//		YH		Yukang Han
//
/////////////////////////////////////////////
//
// Copyright (c) 2018 Innovent Technology Limited.
// All rigths reserved.
// 
// This software is protected by national and international copyright and
// other laws. Unauthorised use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
// 
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Innovent Technology Limited.
// 
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "QScanCommand.h"
#include "UsageEnvironment.hh"
#include "DigestAuthentication.hh"
//////////////////////////////////////////////////////////////////////////
QScanCommand::QScanCommand(class UsageEnvironment* env, const char* progName):
m_env(env),
m_progName(progName),
m_bWindow(false)
{

}
//////////////////////////////////////////////////////////////////////////
QScanCommand::~QScanCommand()
{

}
//////////////////////////////////////////////////////////////////////////
bool QScanCommand::VerifyCommandNum(int argc)
{
	return argc < 2 ? false : true;
}
//////////////////////////////////////////////////////////////////////////
void QScanCommand::operator()(int argc, char** argv)
{
	CommandResult Result = { 0 };

	while (argc > 1) 
	{
		char* const opt = argv[1];
		if (opt[0] != '-') 
		{
			if (argc == 2) break; // only the URL is left
			usage();
		}

		switch (opt[1]) 
		{
			case 'a':
			{
				Result.streamURL = argv[2];
				++argv; --argc;
				break;
			}

			case 'w':
			{
				m_bWindow = true;
				--argc;
				break;
			}

			

			case 'p': 
			{ // specify start port number
				int portArg;
				if (sscanf(argv[2], "%d", &portArg) != 1) 
				{
					usage();
				}
				if (portArg <= 0 || portArg >= 65536 || portArg & 1) 
				{
					*m_env << "bad port number: " << portArg
						<< " (must be even, and in the range (0,65536))\n";
					usage();
				}
				Result.desiredPortNum = (unsigned short)portArg;
				++argv; --argc;
				break;
			}

			case 'u': 
			{ // specify a username and password
				if (argc < 4) usage(); // there's no argv[3] (for the "password")
				Result.username = argv[2];
				Result.password = argv[3];
				argv += 2; argc -= 2;

				Result.ourAuthenticator = new Authenticator(Result.username, Result.password);

				m_kCommandResults.push_back(Result);
				m_kCommandResults.push_back(Result);
				m_kCommandResults.push_back(Result);
				m_kCommandResults.push_back(Result);

				Result = { 0 };
				break;
			}

			default: 
			{
				*m_env << "Invalid option: " << opt << "\n";
				usage();
				break;
			}
		}

		++argv; --argc;
	}
}
//////////////////////////////////////////////////////////////////////////
void QScanCommand::usage()
{
	*m_env << "Usage: " << m_progName << " <rtsp-url-1> ... <rtsp-url-N>\n";
	*m_env << "\t(where each <rtsp-url-i> is a \"rtsp://\" URL)\n";

	shutdown();
}
//////////////////////////////////////////////////////////////////////////
void QScanCommand::shutdown()
{

}
//////////////////////////////////////////////////////////////////////////
bool QScanCommand::isWindow()
{
	return m_bWindow;
}