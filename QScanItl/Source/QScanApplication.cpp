//FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   QScanApplication.cpp
//
// PROJECT:     QScanItl
//
// Application Layer for QScanItl
// 
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//		YH		Yukang Han
//
/////////////////////////////////////////////
//
// Copyright (c) 2018 Innovent Technology Limited.
// All rigths reserved.
// 
// This software is protected by national and international copyright and
// other laws. Unauthorised use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
// 
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Innovent Technology Limited.
// 
///////////////////////////////////////////////////////////////////////////////
#include "QScanApplication.h"
#include "QScanWindow.h"
#include "QScanCommand.h"
#include "QScanRTSPClient.h"
#include "QScanRTSPCallback.h"
#include "QScanUsageEnvironment.h"
#include "QScanHalconInterface.h"

extern "C"
{
#include <libavcodec/avcodec.h>
}

//////////////////////////////////////////////////////////////////////////
QScanApplication::QScanApplication()
{
	m_bOverallSucceeded = false;
}
//////////////////////////////////////////////////////////////////////////
QScanApplication::~QScanApplication()
{

}
//////////////////////////////////////////////////////////////////////////
bool QScanApplication::Initialize(int argc, char** argv)
{
	if (!InitializeEnvironment(argv[0]))
		return false;

	if (!InitializeCommand(argc, argv))
		return false;

	if (!InitializeCodec())
		return false;

	if (!InitializeWindow())
		return false;

	if (!InitializeHalcon())
		return false;

	return true;
}
//////////////////////////////////////////////////////////////////////////
bool QScanApplication::UnInitialize()
{
	return true;
}
//////////////////////////////////////////////////////////////////////////
bool QScanApplication::Run()
{
	bool quit = false;
	
	while (!quit)
	{
		quit = !m_pQScanWindow->HandleWindowEvents();

		m_pEnvironment->taskScheduler().doEventLoop(nullptr);

		if (m_bOverallSucceeded)
		{
			PreUpdate(0);
			Update(0);
			PostUpdate(0);

			PreRender(0);
			Render(0);
			PostRender(0);
		}
	}

	return true;
}
//////////////////////////////////////////////////////////////////////////
void QScanApplication::PreUpdate(unsigned int nDelta)
{
	if (m_pQScanWindow != nullptr)
	{
		m_pQScanWindow->PreUpdate(nDelta);
	}

	for (auto rtspClient : m_ClientList)
	{
		ourRTSPClient* ourClient = dynamic_cast<ourRTSPClient*>(rtspClient);
		if (ourClient != nullptr)
		{
			ourClient->PreUpdate(nDelta);
		}
	}
}
//////////////////////////////////////////////////////////////////////////
void QScanApplication::Update(unsigned int nDelta)
{
	if (m_pQScanWindow != nullptr)
	{
		m_pQScanWindow->Update(nDelta);
	}

	for (auto rtspClient : m_ClientList)
	{
		ourRTSPClient* ourClient = dynamic_cast<ourRTSPClient*>(rtspClient);
		if (ourClient != nullptr)
		{
			ourClient->Update(nDelta);
		}
	}
}
//////////////////////////////////////////////////////////////////////////
void QScanApplication::PostUpdate(unsigned int nDelta)
{
	if (m_pQScanWindow != nullptr)
	{
		m_pQScanWindow->PostUpdate(nDelta);
	}

	for (auto rtspClient : m_ClientList)
	{
		ourRTSPClient* ourClient = dynamic_cast<ourRTSPClient*>(rtspClient);
		if (ourClient != nullptr)
		{
			ourClient->PostUpdate(nDelta);
		}
	}
}
//////////////////////////////////////////////////////////////////////////
void QScanApplication::PreRender(unsigned int nDelta)
{
	if (m_pQScanWindow != nullptr)
	{
		m_pQScanWindow->PreRender(nDelta);
	}

	for (auto rtspClient : m_ClientList)
	{
		ourRTSPClient* ourClient = dynamic_cast<ourRTSPClient*>(rtspClient);
		if (ourClient != nullptr)
		{
			ourClient->PreRender(nDelta);
		}
	}
}
//////////////////////////////////////////////////////////////////////////
void QScanApplication::Render(unsigned int nDelta)
{
	if (m_pQScanWindow != nullptr)
	{
		m_pQScanWindow->Render(nDelta);
	}

	for (auto rtspClient : m_ClientList)
	{
		ourRTSPClient* ourClient = dynamic_cast<ourRTSPClient*>(rtspClient);
		if (ourClient != nullptr)
		{
			ourClient->Render(nDelta);
		}
	}
}
//////////////////////////////////////////////////////////////////////////
void QScanApplication::PostRender(unsigned int nDelta)
{
	if (m_pQScanWindow != nullptr)
	{
		m_pQScanWindow->PostRender(nDelta);
	}

	for (auto rtspClient : m_ClientList)
	{
		ourRTSPClient* ourClient = dynamic_cast<ourRTSPClient*>(rtspClient);
		if (ourClient != nullptr)
		{
			ourClient->PostRender(nDelta);
		}
	}
}
//////////////////////////////////////////////////////////////////////////
void QScanApplication::AddRTSPClient(RTSPClient* client)
{
	m_ClientList.push_back(client);
}
//////////////////////////////////////////////////////////////////////////
void QScanApplication::RemoveRTSPClient(RTSPClient* client)
{
	m_ClientList.remove(client);
}
//////////////////////////////////////////////////////////////////////////
#define RTSP_CLIENT_VERBOSITY_LEVEL 1 // by default, print verbose output from each "RTSPClient"
//////////////////////////////////////////////////////////////////////////
bool QScanApplication::OpenURL()
{
	for (auto i : m_QScanCommand->m_kCommandResults)
	{
		RTSPClient* rtspClient = ourRTSPClient::createNew(*m_pEnvironment, i.streamURL, RTSP_CLIENT_VERBOSITY_LEVEL, m_progName);

		if (rtspClient == NULL)
		{
			(*m_pEnvironment) << "Failed to create a RTSP client for URL \"" << i.streamURL << "\": " << m_pEnvironment->getResultMsg() << "\n";
			continue;
		}

		rtspClient->sendOptionsCommand(continueAfterOPTIONS, i.ourAuthenticator);

		QScanApplication::get().AddRTSPClient(rtspClient);
	}

	return true;
}
//////////////////////////////////////////////////////////////////////////
bool QScanApplication::OpenWindow()
{
	if (!m_QScanCommand->isWindow()) return true;

	return m_pQScanWindow != nullptr ? m_pQScanWindow->OpenSDLWindow() : false;
}
//////////////////////////////////////////////////////////////////////////
void QScanApplication::FeedHalconImage(unsigned int nWidth, unsigned int nHeight, unsigned char* pImagePixel, unsigned int nImagePixelSize)
{
	revHalcon(nWidth, nHeight, pImagePixel, nImagePixelSize);
}
//////////////////////////////////////////////////////////////////////////
void QScanApplication::NotifySuccessOrFailure()
{
	m_bOverallSucceeded = IsOverallSucceeded();

	if (m_bOverallSucceeded)
	{
		SetupWindowDisplay();
	}
}
//////////////////////////////////////////////////////////////////////////
bool QScanApplication::InitializeEnvironment(const char* progName)
{
	if (m_pScheduler == nullptr && m_pEnvironment == nullptr)
	{
		m_progName = progName;

		m_pScheduler = QScanTaskScheduler::createNew();
		m_pEnvironment = QScanUsageEnvironment::createNew(*m_pScheduler);
		return true;
	}

	return false;
}
//////////////////////////////////////////////////////////////////////////
bool QScanApplication::UnInitializeEnvironment()
{
	return true;
}
//////////////////////////////////////////////////////////////////////////
bool QScanApplication::InitializeCommand(int argc, char** argv)
{
	if (m_QScanCommand == nullptr)
	{
		m_QScanCommand = new QScanCommand(m_pEnvironment, argv[0]);

		if (!m_QScanCommand->VerifyCommandNum(argc))
			return false;

		(*m_QScanCommand)(argc, argv);

		return true;
	}
	
	return false;
}
//////////////////////////////////////////////////////////////////////////
bool QScanApplication::InitializeCodec()
{
	avcodec_register_all();

	return true;
}
//////////////////////////////////////////////////////////////////////////
bool QScanApplication::UnInitializeCodec()
{
	return true;
}
//////////////////////////////////////////////////////////////////////////
bool QScanApplication::InitializeWindow()
{
	if (!m_QScanCommand->isWindow()) return true;

	if (m_pQScanWindow == nullptr)
	{
		m_pQScanWindow = new QScanWindow();
		return true;
	}

	return false;
}
//////////////////////////////////////////////////////////////////////////
bool QScanApplication::UnInitializeWindow()
{
	if (m_pQScanWindow != nullptr)
	{
		delete m_pQScanWindow;
		m_pQScanWindow = nullptr;
		return true;
	}

	return false;
}
//////////////////////////////////////////////////////////////////////////
bool QScanApplication::InitializeHalcon()
{
	newHalcon();
	initHalcon();
	return true;
}
//////////////////////////////////////////////////////////////////////////
bool QScanApplication::UnInitializeHalcon()
{
	delHalcon();
	return true;
}
//////////////////////////////////////////////////////////////////////////
void QScanApplication::SetupWindowDisplay()
{
	int nIndex = 0;
	for (auto rtspClient : m_ClientList)
	{
		ourRTSPClient* ourClient = dynamic_cast<ourRTSPClient*>(rtspClient);
		if (ourClient != nullptr && m_pQScanWindow != nullptr)
		{
			ourClient->scs.ffmpeg.CreateDisplay(m_pQScanWindow->CreateDisplay(nIndex++));
		}
	}
}
//////////////////////////////////////////////////////////////////////////
bool QScanApplication::IsOverallSucceeded()
{
	for (auto rtspClient : m_ClientList)
	{
		ourRTSPClient* ourClient = dynamic_cast<ourRTSPClient*>(rtspClient);
		if (ourClient != nullptr)
		{
			if (!ourClient->scs.bSuccess)
				return false;
		}
	}

	return true;
}