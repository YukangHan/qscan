//FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   QScanWindow.cpp
//
// PROJECT:     QScanItl
//
// Wrapper for SDL2
// 
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//		YH		Yukang Han
//
/////////////////////////////////////////////
//
// Copyright (c) 2018 Innovent Technology Limited.
// All rigths reserved.
// 
// This software is protected by national and international copyright and
// other laws. Unauthorised use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
// 
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Innovent Technology Limited.
// 
///////////////////////////////////////////////////////////////////////////////

#include "QScanWindow.h"
#include "QScanWindowDisplay.h"
#include <SDL.h>

extern "C"
{
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
#include <libavutil/frame.h>
}

//////////////////////////////////////////////////////////////////////////
const unsigned short DEFAULT_DISPLAY_POS[4][2] = { { 0, 0 }, { 640, 0 }, { 0, 360 }, { 640, 360 } };
//const unsigned short DEFAULT_DISPLAY_POS[12][2] = { { 0, 0 }, { 320, 0 }, { 640, 0 }, { 960, 0 }, { 0, 240 }, { 320, 240 }, { 640, 240 }, { 960, 240 }, { 0, 480 }, { 320, 480 }, { 640, 480 }, { 960, 480 } };
//////////////////////////////////////////////////////////////////////////
unsigned short GetX(unsigned short nIndex)
{
	return nIndex < 12 ? DEFAULT_DISPLAY_POS[nIndex][0] : 0;
}
//////////////////////////////////////////////////////////////////////////
unsigned short GetY(unsigned short nIndex)
{
	return nIndex < 12 ? DEFAULT_DISPLAY_POS[nIndex][1] : 0;
}
//////////////////////////////////////////////////////////////////////////
QScanWindow::QScanWindow(unsigned short width, unsigned short height) :
m_nWidth(width),
m_nHeight(height)
{
	
}
//////////////////////////////////////////////////////////////////////////
QScanWindow::~QScanWindow()
{
	
}
//////////////////////////////////////////////////////////////////////////
QScanWindowDisplay* QScanWindow::CreateDisplay(unsigned short nIndex)
{
	QScanWindowDisplay* pDisplay = new QScanWindowDisplay();

	pDisplay->InitlizeDisplay(GetWidth(), GetHeight(), GetX(nIndex), GetY(nIndex), SUBDISPLAY_WIDTH, SUBDISPLAY_HEIGHT, sdlRenderer);

	return pDisplay;
}
//////////////////////////////////////////////////////////////////////////
bool QScanWindow::OpenSDLWindow()
{
	if (SDL_Init(SDL_INIT_VIDEO))
	{
		return false;
	}

	screen = SDL_CreateWindow("QScanItl", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, GetWidth(), GetHeight(), SDL_WINDOW_RESIZABLE);

	if (!screen)
	{
		return false;
	}

	sdlRenderer = SDL_CreateRenderer(screen, -1, 0);

	if (!sdlRenderer)
	{
		return false;
	}

	return true;
}
//////////////////////////////////////////////////////////////////////////
bool QScanWindow::HandleWindowEvents()
{
	SDL_Event event;
	while (SDL_PollEvent(&event))
		if (event.type == SDL_QUIT)
			return false;

	return true;
}
//////////////////////////////////////////////////////////////////////////
void QScanWindow::PreUpdate(unsigned int nDelta)
{

}
//////////////////////////////////////////////////////////////////////////
void QScanWindow::Update(unsigned int nDelta)
{

}
//////////////////////////////////////////////////////////////////////////
void QScanWindow::PostUpdate(unsigned int nDelta)
{

}
//////////////////////////////////////////////////////////////////////////
void QScanWindow::PreRender(unsigned int nDelta)
{
	SDL_RenderClear(sdlRenderer);
}
//////////////////////////////////////////////////////////////////////////
void QScanWindow::Render(unsigned int nDelta)
{

}
//////////////////////////////////////////////////////////////////////////
void QScanWindow::PostRender(unsigned int nDelta)
{
	SDL_RenderPresent(sdlRenderer);
}


