//FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   QScanUsageEnvironment.cpp
//
// PROJECT:     QScanItl
//
// Implementation of UsageEnvironment
// 
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//		YH		Yukang Han
//
/////////////////////////////////////////////
//
// Copyright (c) 2018 Innovent Technology Limited.
// All rigths reserved.
// 
// This software is protected by national and international copyright and
// other laws. Unauthorised use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
// 
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Innovent Technology Limited.
// 
///////////////////////////////////////////////////////////////////////////////

#include "QScanUsageEnvironment.h"

//////////////////////////////////////////////////////////////////////////
QScanUsageEnvironment::QScanUsageEnvironment(TaskScheduler& taskScheduler)
:BasicUsageEnvironment(taskScheduler)
{

}
//////////////////////////////////////////////////////////////////////////
QScanUsageEnvironment* QScanUsageEnvironment::createNew(TaskScheduler& taskScheduler)
{
	return new QScanUsageEnvironment(taskScheduler);
}
//////////////////////////////////////////////////////////////////////////
QScanTaskScheduler::QScanTaskScheduler(unsigned maxSchedulerGranularity)
:BasicTaskScheduler(maxSchedulerGranularity)
{

}
//////////////////////////////////////////////////////////////////////////
QScanTaskScheduler* QScanTaskScheduler::createNew(unsigned maxSchedulerGranularity)
{
	return new QScanTaskScheduler(maxSchedulerGranularity);
}
//////////////////////////////////////////////////////////////////////////
void QScanTaskScheduler::doEventLoop(char volatile* watchVariable)
{
	SingleStep(0);
}