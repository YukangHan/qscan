//FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   QScanFFmpeg.cpp
//
// PROJECT:     QScanItl
//
// Wrapper for ffmpeg
// 
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//		YH		Yukang Han
//
/////////////////////////////////////////////
//
// Copyright (c) 2018 Innovent Technology Limited.
// All rigths reserved.
// 
// This software is protected by national and international copyright and
// other laws. Unauthorised use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
// 
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Innovent Technology Limited.
// 
///////////////////////////////////////////////////////////////////////////////
#include "QScanFFmpeg.h"
#include "QScanDummySink.h"
#include "QScanCommonDefine.h"
#include "QScanGlobalVariable.h"
#include "QScanFourCC.h"
#include "QScanWindow.h"
#include "QScanApplication.h"
#include "QScanWindowDisplay.h"

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
}

extern "C"
{
#include <libavutil/imgutils.h>
#include <libavutil/frame.h>
}

//////////////////////////////////////////////////////////////////////////
static uint32_t ffmpeg_CodecTag(vlc_fourcc_t fcc)
{
	uint8_t *p = (uint8_t*)&fcc;
	return p[0] | (p[1] << 8) | (p[2] << 16) | (p[3] << 24);
}
//////////////////////////////////////////////////////////////////////////
static int decode(AVCodecContext *dec_ctx, AVFrame *frame, AVPacket *pkt)
{
	int ret = avcodec_send_packet(dec_ctx, pkt);
	if (ret < 0)
	{
		return ret;
	}

	while (ret >= 0) 
	{
		ret = avcodec_receive_frame(dec_ctx, frame);
		if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
		{
			return ret;
		}
		else if (ret < 0)
		{
			return ret;
		}
		if (ret >= 0)
		{
			return ret;
		}
	}

	return ret;
}
//////////////////////////////////////////////////////////////////////////
QScanFFmpeg::QScanFFmpeg()
{
	m_pFrame = nullptr; m_pCodec = nullptr; m_pCodecContext = nullptr; 
	m_pCodecParser = nullptr; m_pFormatContext = nullptr; m_pDisplay = nullptr;
	
	sws_ctxRGB = nullptr; m_pFrameRGB = nullptr; m_pOutBufferRGB = nullptr;

	m_nOutBufferSize = 0;

	m_bHasReceived = false;
}
//////////////////////////////////////////////////////////////////////////
QScanFFmpeg::~QScanFFmpeg()
{
	DestroyCodec();
	DestroyDisplay();
}
//////////////////////////////////////////////////////////////////////////
bool QScanFFmpeg::CreateCodec()
{
	unsigned int i_codec_id;
	int i_cat;
	const char *psz_namecodec;
	int ret;

	if (!GetFfmpegCodec(G_FORMAT_IN.i_codec, &i_cat, &i_codec_id, &psz_namecodec) || i_cat == UNKNOWN_ES)
	{
		return false;
	}

	av_init_packet(&m_kPacket);
	m_kPacket.flags |= AV_PKT_FLAG_KEY;
	m_kPacket.pts = m_kPacket.dts = 0;

	m_pFormatContext = avformat_alloc_context();
	if (!m_pFormatContext)
	{
		return false;
	}

	m_pCodec = avcodec_find_decoder((AVCodecID)i_codec_id);
	if (!m_pCodec)
	{
		return false;
	}

	m_pCodecContext = avcodec_alloc_context3(m_pCodec);
	if (!m_pCodecContext)
	{
		return false;
	}

	m_pCodecParser = av_parser_init((AVCodecID)i_codec_id);
	if (!m_pCodecParser)
	{
		return false;
	}

	if (!SetupVideoDec((AVCodecID)i_codec_id))
	{
		return false;
	}

	if (!SetupCodecExtra((AVCodecID)i_codec_id))
	{
		return false;
	}

	if (G_FORMAT_IN.i_cat == VIDEO_ES)
	{
		m_pCodecContext->width = G_FORMAT_IN.video.i_visible_width;
		m_pCodecContext->height = G_FORMAT_IN.video.i_visible_height;
		if (m_pCodecContext->width == 0)
			m_pCodecContext->width = G_FORMAT_IN.video.i_width;
		else if (m_pCodecContext->width != G_FORMAT_IN.video.i_width)
			m_pCodecContext->coded_width = G_FORMAT_IN.video.i_width;
		if (m_pCodecContext->height == 0)
			m_pCodecContext->height = G_FORMAT_IN.video.i_height;
		else if (m_pCodecContext->height != G_FORMAT_IN.video.i_height)
			m_pCodecContext->coded_height = G_FORMAT_IN.video.i_height;
		m_pCodecContext->bits_per_coded_sample = G_FORMAT_IN.video.i_bits_per_pixel;
	}

	if (m_pCodec->capabilities & AV_CODEC_CAP_TRUNCATED)
	{
		m_pCodecContext->flags |= AV_CODEC_FLAG_TRUNCATED; // we do not send complete frames
	}

	ret = avcodec_open2(m_pCodecContext, m_pCodec, NULL);
	if (ret < 0)
	{
		return false;
	}

	m_pCodecContext->width = QScanWindow::DEFAULT_WIDTH;
	m_pCodecContext->height = QScanWindow::DEFAULT_HEIGHT;
	m_pCodecContext->pix_fmt = AV_PIX_FMT_YUV420P;

	m_pFrame = av_frame_alloc();
	if (!m_pFrame)
	{
		return false;
	}

	m_pFrameRGB = av_frame_alloc();
	if (!m_pFrameRGB)
	{
		return false;
	}

	m_nOutBufferSize = av_image_get_buffer_size(AV_PIX_FMT_RGB24, m_pCodecContext->width, m_pCodecContext->height, 1);

	m_pOutBufferRGB = (unsigned char*)av_malloc(m_nOutBufferSize);

	if (!m_pOutBufferRGB)
	{
		return false;
	}

	av_image_fill_arrays(m_pFrameRGB->data, m_pFrameRGB->linesize, m_pOutBufferRGB, AV_PIX_FMT_RGB24, m_pCodecContext->width, m_pCodecContext->height, 1);

	sws_ctxRGB = sws_getContext(m_pCodecContext->width, m_pCodecContext->height, AV_PIX_FMT_YUV420P, m_pCodecContext->width, m_pCodecContext->height, AV_PIX_FMT_RGB24, SWS_BICUBIC, NULL, NULL, NULL);

	return true;
}
//////////////////////////////////////////////////////////////////////////
bool QScanFFmpeg::DestroyCodec()
{
	m_pCodec = nullptr;

	if (m_pFormatContext)
	{
		avformat_free_context(m_pFormatContext);
	}

	if (m_pCodecContext)
	{
		avcodec_free_context(&m_pCodecContext);
	}

	if (m_pCodecParser)
	{
		av_parser_close(m_pCodecParser);
	}

	if (m_pFrame)
	{
		av_frame_free(&m_pFrame);
	}

	if (m_pFrameRGB)
	{
		av_frame_free(&m_pFrameRGB);
	}

	return true;
}
//////////////////////////////////////////////////////////////////////////
void QScanFFmpeg::SetSprop(u_int8_t const* prop, unsigned size)
{
	uint8_t *buf;
	uint8_t *buf_start;
	buf = (uint8_t *)malloc(1000);
	buf_start = buf + 4;

	m_kPacket.data = buf;
	m_kPacket.data[0] = 0;
	m_kPacket.data[1] = 0;
	m_kPacket.data[2] = 0;
	m_kPacket.data[3] = 1;
	memcpy(buf_start, prop, size);
	m_kPacket.size = size + 4;
	
	int ret = decode(m_pCodecContext, m_pFrame, &m_kPacket);
}
//////////////////////////////////////////////////////////////////////////
bool QScanFFmpeg::CreateDisplay(QScanWindowDisplay* pDisplay)
{
	m_pDisplay = pDisplay;
	return true;
}
//////////////////////////////////////////////////////////////////////////
bool QScanFFmpeg::DestroyDisplay()
{
	if (m_pDisplay)
	{
		delete m_pDisplay;
		m_pDisplay = nullptr;
	}
	return true;
}
//////////////////////////////////////////////////////////////////////////
bool QScanFFmpeg::ReceiveBuffer(u_int8_t* fReceiveBuffer, unsigned frameSize)
{
	m_kPacket.data = fReceiveBuffer;
	m_kPacket.size = frameSize;

	int ret = decode(m_pCodecContext, m_pFrame, &m_kPacket);

	m_bHasReceived |= (ret == 0);

	if (m_bHasReceived && (ret == 0))
	{
		ConvertToRGB();
		//QScanApplication::get().FeedHalconImage(m_pCodecContext->width, m_pCodecContext->height, m_pDisplay->m_pOutBufferYUV, m_pDisplay->m_nOutBufferSize);
		QScanApplication::get().FeedHalconImage(m_pCodecContext->width, m_pCodecContext->height, m_pOutBufferRGB, m_nOutBufferSize);
	}

	return ret == 0;
}
//////////////////////////////////////////////////////////////////////////
void QScanFFmpeg::Update(unsigned int nDelta)
{

}
//////////////////////////////////////////////////////////////////////////
void QScanFFmpeg::Render(unsigned int nDelta)
{
	if (m_pDisplay != nullptr && m_bHasReceived)
 	{
		m_pDisplay->Render(nDelta, m_pFrame);
 	}
}
//////////////////////////////////////////////////////////////////////////
bool QScanFFmpeg::SetupVideoDec(AVCodecID nCodecID)
{
	m_pCodec->type = AVMEDIA_TYPE_VIDEO;
	m_pCodecContext->codec_type = AVMEDIA_TYPE_VIDEO;
	m_pCodecContext->codec_id = nCodecID;

	/* ***** Fill p_context with init values ***** */
	m_pCodecContext->codec_tag = ffmpeg_CodecTag(G_FORMAT_IN.i_original_fourcc ? G_FORMAT_IN.i_original_fourcc : G_FORMAT_IN.i_codec);			// sunqueen modify

	/* ***** Output always the frames ***** */
#if LIBAVCODEC_VERSION_CHECK(55, 23, 1, 40, 101)
	m_pCodecContext->flags |= AV_CODEC_FLAG_OUTPUT_CORRUPT;
#endif

	return true;
}
//////////////////////////////////////////////////////////////////////////
bool QScanFFmpeg::SetupCodecExtra(AVCodecID nCodecID)
{
	int i_size = G_FORMAT_IN.i_extra;

	if (!i_size) return false;

	if (nCodecID == AV_CODEC_ID_SVQ3)
	{
		uint8_t *p;

		m_pCodecContext->extradata_size = i_size + 12;
		p = m_pCodecContext->extradata = (uint8_t*)
			av_malloc(m_pCodecContext->extradata_size +
				AV_INPUT_BUFFER_PADDING_SIZE);
		if (!p)
			return true;

		memcpy(&p[0], "SVQ3", 4);
		memset(&p[4], 0, 8);
		memcpy(&p[12], G_FORMAT_IN.p_extra, i_size);

		/* Now remove all atoms before the SMI one */
		if (m_pCodecContext->extradata_size > 0x5a &&
			strncmp((char*)&p[0x56], "SMI ", 4))
		{
			uint8_t *psz = &p[0x52];

			while (psz < &p[m_pCodecContext->extradata_size - 8])
			{
				int i_size = GetDWBE(psz);
				if (i_size <= 1)
				{
					/* FIXME handle 1 as long size */
					break;
				}
				if (!strncmp((char*)&psz[4], "SMI ", 4))
				{
					memmove(&p[0x52], psz,
						&p[m_pCodecContext->extradata_size] - psz);
					break;
				}

				psz += i_size;
			}
		}
	}
	else
	{
		m_pCodecContext->extradata_size = i_size;
		m_pCodecContext->extradata = (uint8_t*)
			av_malloc(i_size + AV_INPUT_BUFFER_PADDING_SIZE);
		if (m_pCodecContext->extradata)
		{
			memcpy(m_pCodecContext->extradata,
				G_FORMAT_IN.p_extra, i_size);
			memset(m_pCodecContext->extradata + i_size,
				0, AV_INPUT_BUFFER_PADDING_SIZE);
		}
	}

	return true;
}
//////////////////////////////////////////////////////////////////////////
bool QScanFFmpeg::send(AVPacket* packet)
{
	auto ret = avcodec_send_packet(m_pCodecContext, packet);
	if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
	{
		return false;
	}
	else 
	{
		return true;
	}
}
//////////////////////////////////////////////////////////////////////////
bool QScanFFmpeg::receive(AVFrame* frame)
{
	auto ret = avcodec_receive_frame(m_pCodecContext, frame);
	if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
	{
		return false;
	}
	else 
	{
		return true;
	}
}
//////////////////////////////////////////////////////////////////////////
bool QScanFFmpeg::ConvertToRGB()
{

	int v = sws_scale(sws_ctxRGB, (const uint8_t* const*)m_pFrame->data, m_pFrame->linesize, 0, m_pCodecContext->height, m_pFrameRGB->data, m_pFrameRGB->linesize);

	return true;
}