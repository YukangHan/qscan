//FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   QScanRTSPClient.cpp
//
// PROJECT:     QScanItl
//
// Implementation of RTSP client
// 
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//		YH		Yukang Han
//
/////////////////////////////////////////////
//
// Copyright (c) 2018 Innovent Technology Limited.
// All rigths reserved.
// 
// This software is protected by national and international copyright and
// other laws. Unauthorised use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
// 
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Innovent Technology Limited.
// 
///////////////////////////////////////////////////////////////////////////////

#include "QScanRTSPClient.h"

// Implementation of "ourRTSPClient":
//////////////////////////////////////////////////////////////////////////
ourRTSPClient* ourRTSPClient::createNew(UsageEnvironment& env, char const* rtspURL, int verbosityLevel, char const* applicationName, portNumBits tunnelOverHTTPPortNum)
{
	return new ourRTSPClient(env, rtspURL, verbosityLevel, applicationName, tunnelOverHTTPPortNum);
}
//////////////////////////////////////////////////////////////////////////
ourRTSPClient::ourRTSPClient(UsageEnvironment& env, char const* rtspURL, int verbosityLevel, char const* applicationName, portNumBits tunnelOverHTTPPortNum)
	: RTSPClient(env, rtspURL, verbosityLevel, applicationName, tunnelOverHTTPPortNum, -1)
{

}
//////////////////////////////////////////////////////////////////////////
ourRTSPClient::~ourRTSPClient()
{

}
//////////////////////////////////////////////////////////////////////////
void ourRTSPClient::PreUpdate(unsigned int nDelta)
{
	scs.ffmpeg.PreUpdate(nDelta);
}
//////////////////////////////////////////////////////////////////////////
void ourRTSPClient::Update(unsigned int nDelta)
{
	scs.ffmpeg.Update(nDelta);
}
//////////////////////////////////////////////////////////////////////////
void ourRTSPClient::PostUpdate(unsigned int nDelta)
{
	scs.ffmpeg.PostUpdate(nDelta);
}
//////////////////////////////////////////////////////////////////////////
void ourRTSPClient::PreRender(unsigned int nDelta)
{
	scs.ffmpeg.PreRender(nDelta);
}
//////////////////////////////////////////////////////////////////////////
void ourRTSPClient::Render(unsigned int nDelta)
{
	scs.ffmpeg.Render(nDelta);
}
//////////////////////////////////////////////////////////////////////////
void ourRTSPClient::PostRender(unsigned int nDelta)
{
	scs.ffmpeg.PostRender(nDelta);
}