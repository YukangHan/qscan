//FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   QScanRTSPCallback.cpp
//
// PROJECT:     QScanItl
//
// call-backs of RTSP
// 
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//		YH		Yukang Han
//
/////////////////////////////////////////////
//
// Copyright (c) 2018 Innovent Technology Limited.
// All rigths reserved.
// 
// This software is protected by national and international copyright and
// other laws. Unauthorised use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
// 
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Innovent Technology Limited.
// 
///////////////////////////////////////////////////////////////////////////////

#include <GroupsockHelper.hh>

#include "QScanRTSPCallback.h"
#include "QScanStreamClientState.h"
#include "QScanRTSPClient.h"
#include "QScanDummySink.h"
#include "QScanRTSPClient.h"
#include "QScanFourCC.h"
#include "QScanGlobalVariable.h"
#include "QScanFFmpeg.h"
#include "QScanApplication.h"


// A function that outputs a string that identifies each stream (for debugging output).  Modify this if you wish:
UsageEnvironment& operator<<(UsageEnvironment& env, const RTSPClient& rtspClient)
{
	return env << "[URL:\"" << rtspClient.url() << "\"]: ";
}

// A function that outputs a string that identifies each subsession (for debugging output).  Modify this if you wish:
UsageEnvironment& operator<<(UsageEnvironment& env, const MediaSubsession& subsession)
{
	return env << subsession.mediumName() << "/" << subsession.codecName();
}

// Implementation of the RTSP 'response handlers':

void continueAfterOPTIONS(RTSPClient* rtspClient, int resultCode, char* resultString)
{
	do {
		UsageEnvironment& env = rtspClient->envir(); // alias
		StreamClientState& scs = ((ourRTSPClient*)rtspClient)->scs; // alias

		if (resultCode != 0)
		{
			env << *rtspClient << "Failed to get the OPTIONS: " << resultString << "\n";
			delete[] resultString;
			break;
		}

		delete[] resultString;

		// Next, send a RTSP "DESCRIBE" command, to get a SDP description for the stream.
		// Note that this command - like all RTSP commands - is sent asynchronously; we do not block, waiting for a response.
		// Instead, the following function call returns immediately, and we handle the RTSP response later, from within the event loop:
		rtspClient->sendDescribeCommand(continueAfterDESCRIBE);
		return;
	} while (0);

	// An unrecoverable error occurred with this stream.
	shutdownStream(rtspClient);
}

void continueAfterDESCRIBE(RTSPClient* rtspClient, int resultCode, char* resultString)
{
	do {
		UsageEnvironment& env = rtspClient->envir(); // alias
		StreamClientState& scs = ((ourRTSPClient*)rtspClient)->scs; // alias

		if (resultCode != 0) {
			env << *rtspClient << "Failed to get a SDP description: " << resultString << "\n";
			delete[] resultString;
			break;
		}

		char* const sdpDescription = resultString;
		env << *rtspClient << "Got a SDP description:\n" << sdpDescription << "\n";

		// Create a media session object from this SDP description:
		scs.session = MediaSession::createNew(env, sdpDescription);
		delete[] sdpDescription; // because we don't need it anymore
		if (scs.session == NULL) {
			env << *rtspClient << "Failed to create a MediaSession object from the SDP description: " << env.getResultMsg() << "\n";
			break;
		}
		else if (!scs.session->hasSubsessions()) {
			env << *rtspClient << "This session has no media subsessions (i.e., no \"m=\" lines)\n";
			break;
		}

//////////////////////////////////////////////////////////////////////////
		unsigned int   i_receive_buffer = 0;
		unsigned const thresh = 200000; /* RTP reorder threshold .2 second (default .1) */

		/* Initialise each media subsession */
		MediaSubsession* sub = NULL;
		MediaSubsessionIterator* iter = new MediaSubsessionIterator(*scs.session);
		while ((sub = iter->next()) != NULL)
		{
			/* Value taken from mplayer */
			if (!strcmp(sub->mediumName(), "audio"))
				i_receive_buffer = 100000;
			else if (!strcmp(sub->mediumName(), "video"))
			{
				i_receive_buffer = 2000000;
			}
			else if (!strcmp(sub->mediumName(), "text"))
				;
			else continue;

			if (sub->rtpSource() != NULL)
			{
				int fd = sub->rtpSource()->RTPgs()->socketNum();

				/* Increase the buffer size */
				if (i_receive_buffer > 0)
					increaseReceiveBufferTo(env, fd, i_receive_buffer);

				/* Increase the RTP reorder timebuffer just a bit */
				sub->rtpSource()->setPacketReorderingThresholdTime(thresh);
			}

			if (!strcmp(sub->mediumName(), "video"))
			{
				G_FORMAT_IN.i_cat = VIDEO_ES;
				G_FORMAT_IN.i_codec = VLC_FOURCC('u', 'n', 'd', 'f');
				G_FORMAT_IN.i_original_fourcc = 0;
				G_FORMAT_IN.i_profile = -1;
				G_FORMAT_IN.i_level = -1;
				G_FORMAT_IN.i_id = -1;
				G_FORMAT_IN.i_group = 0;
				G_FORMAT_IN.i_priority = ES_PRIORITY_SELECTABLE_MIN;
				G_FORMAT_IN.psz_language = NULL;
				G_FORMAT_IN.psz_description = NULL;

				G_FORMAT_IN.i_extra_languages = 0;
				G_FORMAT_IN.p_extra_languages = NULL;

				memset(&G_FORMAT_IN.video, 0, sizeof(video_format_t));

				G_FORMAT_IN.b_packetized = true;
				G_FORMAT_IN.i_bitrate = 0;
				G_FORMAT_IN.i_extra = 0;
				G_FORMAT_IN.p_extra = NULL;

				if (!strcmp(sub->codecName(), "MPV"))
				{
					G_FORMAT_IN.i_codec = VLC_CODEC_MPGV;
					G_FORMAT_IN.b_packetized = false;
				}
				else if (!strcmp(sub->codecName(), "H263") ||
					!strcmp(sub->codecName(), "H263-1998") ||
					!strcmp(sub->codecName(), "H263-2000"))
				{
					G_FORMAT_IN.i_codec = VLC_CODEC_H263;
				}
				else if (!strcmp(sub->codecName(), "H261"))
				{
					G_FORMAT_IN.i_codec = VLC_CODEC_H261;
				}
				else if (!strcmp(sub->codecName(), "H264"))
				{
					unsigned int i_extra = 0;
					uint8_t      *p_extra = NULL;

					G_FORMAT_IN.i_codec = VLC_CODEC_H264;
					G_FORMAT_IN.b_packetized = false;

					if ((p_extra = parseH264ConfigStr(sub->fmtp_spropparametersets(),
						i_extra)))
					{
						G_FORMAT_IN.i_extra = i_extra;
						G_FORMAT_IN.p_extra = malloc(i_extra);
						memcpy(G_FORMAT_IN.p_extra, p_extra, i_extra);

						delete[] p_extra;
					}
				}
#if LIVEMEDIA_LIBRARY_VERSION_INT >= 1393372800 // 2014.02.26
				else if (!strcmp(sub->codecName(), "H265"))
				{
					unsigned int i_extra1 = 0, i_extra2 = 0, i_extra3 = 0, i_extraTot;
					uint8_t      *p_extra1 = NULL, *p_extra2 = NULL, *p_extra3 = NULL;

					G_FORMAT_IN.i_codec = VLC_CODEC_HEVC;
					G_FORMAT_IN.b_packetized = false;

					p_extra1 = parseH264ConfigStr(sub->fmtp_spropvps(), i_extra1);
					p_extra2 = parseH264ConfigStr(sub->fmtp_spropsps(), i_extra2);
					p_extra3 = parseH264ConfigStr(sub->fmtp_sproppps(), i_extra3);
					i_extraTot = i_extra1 + i_extra2 + i_extra3;
					if (i_extraTot > 0)
					{
						G_FORMAT_IN.i_extra = i_extraTot;
						G_FORMAT_IN.p_extra = malloc(i_extraTot);
						if (p_extra1)
						{
							memcpy(G_FORMAT_IN.p_extra, p_extra1, i_extra1);
						}
						if (p_extra2)
						{
							memcpy(((char*)G_FORMAT_IN.p_extra) + i_extra1, p_extra2, i_extra2);
						}
						if (p_extra3)
						{
							memcpy(((char*)G_FORMAT_IN.p_extra) + i_extra1 + i_extra2, p_extra3, i_extra3);
						}

						delete[] p_extra1; delete[] p_extra2; delete[] p_extra3;
					}
				}
#endif
				else if (!strcmp(sub->codecName(), "JPEG"))
				{
					G_FORMAT_IN.i_codec = VLC_CODEC_MJPG;
				}
				else if (!strcmp(sub->codecName(), "MP4V-ES"))
				{
					unsigned int i_extra;
					uint8_t      *p_extra;

					G_FORMAT_IN.i_codec = VLC_CODEC_MP4V;

					if ((p_extra = parseGeneralConfigStr(sub->fmtp_config(),
						i_extra)))
					{
						G_FORMAT_IN.i_extra = i_extra;
						G_FORMAT_IN.p_extra = malloc(i_extra);
						memcpy(G_FORMAT_IN.p_extra, p_extra, i_extra);
						delete[] p_extra;
					}
				}
				else if (!strcmp(sub->codecName(), "VP8"))
				{
					G_FORMAT_IN.i_codec = VLC_CODEC_VP8;
				}
			}
		}
		delete iter;
//////////////////////////////////////////////////////////////////////////

		// Then, create and set up our data source objects for the session.  We do this by iterating over the session's 'subsessions',
		// calling "MediaSubsession::initiate()", and then sending a RTSP "SETUP" command, on each one.
		// (Each 'subsession' will have its own data source.)
		scs.iter = new MediaSubsessionIterator(*scs.session);
		setupNextSubsession(rtspClient);
		return;
	} while (0);

	// An unrecoverable error occurred with this stream.
	shutdownStream(rtspClient);
}

// By default, we request that the server stream its data using RTP/UDP.
// If, instead, you want to request that the server stream via RTP-over-TCP, change the following to True:
#define REQUEST_STREAMING_OVER_TCP False

void setupNextSubsession(RTSPClient* rtspClient)
{
	UsageEnvironment& env = rtspClient->envir(); // alias
	StreamClientState& scs = ((ourRTSPClient*)rtspClient)->scs; // alias

	scs.subsession = scs.iter->next();
	if (scs.subsession != NULL) {
		if (!scs.subsession->initiate()) {
			env << *rtspClient << "Failed to initiate the \"" << *scs.subsession << "\" subsession: " << env.getResultMsg() << "\n";
			setupNextSubsession(rtspClient); // give up on this subsession; go to the next one
		}
		else {
			env << *rtspClient << "Initiated the \"" << *scs.subsession << "\" subsession (";
			if (scs.subsession->rtcpIsMuxed()) {
				env << "client port " << scs.subsession->clientPortNum();
			}
			else {
				env << "client ports " << scs.subsession->clientPortNum() << "-" << scs.subsession->clientPortNum() + 1;
			}
			env << ")\n";

			// Continue setting up this subsession, by sending a RTSP "SETUP" command:
			rtspClient->sendSetupCommand(*scs.subsession, continueAfterSETUP, False, REQUEST_STREAMING_OVER_TCP);
		}
		return;
	}

	// We've finished setting up all of the subsessions.  Now, send a RTSP "PLAY" command to start the streaming:
	if (scs.session->absStartTime() != NULL) {
		// Special case: The stream is indexed by 'absolute' time, so send an appropriate "PLAY" command:
		rtspClient->sendPlayCommand(*scs.session, continueAfterPLAY, scs.session->absStartTime(), scs.session->absEndTime());
	}
	else {
		scs.duration = scs.session->playEndTime() - scs.session->playStartTime();
		rtspClient->sendPlayCommand(*scs.session, continueAfterPLAY);
	}
}

void continueAfterSETUP(RTSPClient* rtspClient, int resultCode, char* resultString)
{
	do {
		UsageEnvironment& env = rtspClient->envir(); // alias
		StreamClientState& scs = ((ourRTSPClient*)rtspClient)->scs; // alias
		ourRTSPClient* ourClient = (ourRTSPClient*)rtspClient;

		if (resultCode != 0)
		{
			env << *rtspClient << "Failed to set up the \"" << *scs.subsession << "\" subsession: " << resultString << "\n";
			break;
		}

		env << *rtspClient << "Set up the \"" << *scs.subsession << "\" subsession (";
		if (scs.subsession->rtcpIsMuxed())
		{
			env << "client port " << scs.subsession->clientPortNum();
		}
		else
		{
			env << "client ports " << scs.subsession->clientPortNum() << "-" << scs.subsession->clientPortNum() + 1;
		}
		env << ")\n";

		const char *sprop = scs.subsession->fmtp_spropparametersets();
		uint8_t const* sps = NULL;
		unsigned spsSize = 0;
		uint8_t const* pps = NULL;
		unsigned ppsSize = 0;

		if (sprop != NULL)
		{
			unsigned numSPropRecords;
			SPropRecord* sPropRecords = parseSPropParameterSets(sprop, numSPropRecords);
			for (unsigned i = 0; i < numSPropRecords; ++i)
			{
				if (sPropRecords[i].sPropLength == 0) continue; // bad data
				u_int8_t nal_unit_type = (sPropRecords[i].sPropBytes[0]) & 0x1F;
				if (nal_unit_type == 7/*SPS*/)
				{
					sps = sPropRecords[i].sPropBytes;
					spsSize = sPropRecords[i].sPropLength;
				}
				else if (nal_unit_type == 8/*PPS*/)
				{
					pps = sPropRecords[i].sPropBytes;
					ppsSize = sPropRecords[i].sPropLength;
				}
			}
		}


		scs.ffmpeg.CreateCodec();

		// Having successfully setup the subsession, create a data sink for it, and call "startPlaying()" on it.
		// (This will prepare the data sink to receive data; the actual flow of data from the client won't start happening until later,
		// after we've sent a RTSP "PLAY" command.)

		scs.subsession->sink = DummySink::createNew(env, *scs.subsession, rtspClient->url());
		// perhaps use your own custom "MediaSink" subclass instead
		if (scs.subsession->sink == NULL) {
			env << *rtspClient << "Failed to create a data sink for the \"" << *scs.subsession
				<< "\" subsession: " << env.getResultMsg() << "\n";
			break;
		}

		env << *rtspClient << "Created a data sink for the \"" << *scs.subsession << "\" subsession\n";
		scs.subsession->miscPtr = rtspClient; // a hack to let subsession handler functions get the "RTSPClient" from the subsession 

		if (sps != NULL)
		{
			scs.ffmpeg.SetSprop(sps, spsSize);
		}
		if (pps != NULL)
		{
			scs.ffmpeg.SetSprop(pps, ppsSize);
		}

		scs.subsession->sink->startPlaying(*(scs.subsession->readSource()),
			subsessionAfterPlaying, scs.subsession);
		// Also set a handler to be called if a RTCP "BYE" arrives for this subsession:
		if (scs.subsession->rtcpInstance() != NULL) {
			scs.subsession->rtcpInstance()->setByeHandler(subsessionByeHandler, scs.subsession);
		}
	} while (0);
	delete[] resultString;

	// Set up the next subsession, if any:
	setupNextSubsession(rtspClient);
}

void continueAfterPLAY(RTSPClient* rtspClient, int resultCode, char* resultString)
{
	Boolean success = False;

	do {
		UsageEnvironment& env = rtspClient->envir(); // alias
		StreamClientState& scs = ((ourRTSPClient*)rtspClient)->scs; // alias

		if (resultCode != 0) {
			env << *rtspClient << "Failed to start playing session: " << resultString << "\n";
			break;
		}

		scs.bSuccess = true;
		QScanApplication::get().NotifySuccessOrFailure();

		// Set a timer to be handled at the end of the stream's expected duration (if the stream does not already signal its end
		// using a RTCP "BYE").  This is optional.  If, instead, you want to keep the stream active - e.g., so you can later
		// 'seek' back within it and do another RTSP "PLAY" - then you can omit this code.
		// (Alternatively, if you don't want to receive the entire stream, you could set this timer for some shorter value.)
		if (scs.duration > 0) {
			unsigned const delaySlop = 2; // number of seconds extra to delay, after the stream's expected duration.  (This is optional.)
			scs.duration += delaySlop;
			unsigned uSecsToDelay = (unsigned)(scs.duration * 1000000);
			scs.streamTimerTask = env.taskScheduler().scheduleDelayedTask(uSecsToDelay, (TaskFunc*)streamTimerHandler, rtspClient);
		}

		env << *rtspClient << "Started playing session";
		if (scs.duration > 0) {
			env << " (for up to " << scs.duration << " seconds)";
		}
		env << "...\n";

		success = True;
	} while (0);
	delete[] resultString;

	if (!success) {
		// An unrecoverable error occurred with this stream.
		shutdownStream(rtspClient);
	}
}


// Implementation of the other event handlers:

void subsessionAfterPlaying(void* clientData)
{
	MediaSubsession* subsession = (MediaSubsession*)clientData;
	RTSPClient* rtspClient = (RTSPClient*)(subsession->miscPtr);

	// Begin by closing this subsession's stream:
	Medium::close(subsession->sink);
	subsession->sink = NULL;

	// Next, check whether *all* subsessions' streams have now been closed:
	MediaSession& session = subsession->parentSession();
	MediaSubsessionIterator iter(session);
	while ((subsession = iter.next()) != NULL) {
		if (subsession->sink != NULL) return; // this subsession is still active
	}

	// All subsessions' streams have now been closed, so shutdown the client:
	shutdownStream(rtspClient);
}

void subsessionByeHandler(void* clientData)
{
	MediaSubsession* subsession = (MediaSubsession*)clientData;
	RTSPClient* rtspClient = (RTSPClient*)subsession->miscPtr;
	UsageEnvironment& env = rtspClient->envir(); // alias

	env << *rtspClient << "Received RTCP \"BYE\" on \"" << *subsession << "\" subsession\n";

	// Now act as if the subsession had closed:
	subsessionAfterPlaying(subsession);
}

void streamTimerHandler(void* clientData)
{
	ourRTSPClient* rtspClient = (ourRTSPClient*)clientData;
	StreamClientState& scs = rtspClient->scs; // alias

	scs.streamTimerTask = NULL;

	// Shut down the stream:
	shutdownStream(rtspClient);
}

static unsigned rtspClientCount = 0; // Counts how many streams (i.e., "RTSPClient"s) are currently in use.

void shutdownStream(RTSPClient* rtspClient, int exitCode)
{
	UsageEnvironment& env = rtspClient->envir(); // alias
	StreamClientState& scs = ((ourRTSPClient*)rtspClient)->scs; // alias

	// First, check whether any subsessions have still to be closed:
	if (scs.session != NULL) {
		Boolean someSubsessionsWereActive = False;
		MediaSubsessionIterator iter(*scs.session);
		MediaSubsession* subsession;

		while ((subsession = iter.next()) != NULL) {
			if (subsession->sink != NULL) {
				Medium::close(subsession->sink);
				subsession->sink = NULL;

				if (subsession->rtcpInstance() != NULL) {
					subsession->rtcpInstance()->setByeHandler(NULL, NULL); // in case the server sends a RTCP "BYE" while handling "TEARDOWN"
				}

				someSubsessionsWereActive = True;
			}
		}

		if (someSubsessionsWereActive) {
			// Send a RTSP "TEARDOWN" command, to tell the server to shutdown the stream.
			// Don't bother handling the response to the "TEARDOWN".
			rtspClient->sendTeardownCommand(*scs.session, NULL);
		}
	}

	env << *rtspClient << "Closing the stream.\n";
	Medium::close(rtspClient);
	QScanApplication::get().RemoveRTSPClient(rtspClient);
	QScanApplication::get().NotifySuccessOrFailure();

	// Note that this will also cause this stream's "StreamClientState" structure to get reclaimed.

	if (--rtspClientCount == 0) {
		// The final stream has ended, so exit the application now.
		// (Of course, if you're embedding this code into your own application, you might want to comment this out,
		// and replace it with "eventLoopWatchVariable = 1;", so that we leave the LIVE555 event loop, and continue running "main()".)
		exit(exitCode);
	}
}

void usage(UsageEnvironment& env, char const* progName)
{
	env << "Usage: " << progName << " <rtsp-url-1> ... <rtsp-url-N>\n";
	env << "\t(where each <rtsp-url-i> is a \"rtsp://\" URL)\n";
}

static unsigned char* parseH264ConfigStr(char const* configStr, unsigned int& configSize)
{
	char *dup, *psz;
	size_t i_records = 1;

	configSize = 0;

	if (configStr == NULL || *configStr == '\0')
		return NULL;

	psz = dup = strdup(configStr);

	/* Count the number of commas */
	for (psz = dup; *psz != '\0'; ++psz)
	{
		if (*psz == ',')
		{
			++i_records;
			*psz = '\0';
		}
	}

	size_t configMax = 5 * strlen(dup);
	unsigned char *cfg = new unsigned char[configMax];
	psz = dup;
	for (size_t i = 0; i < i_records; ++i)
	{
		cfg[configSize++] = 0x00;
		cfg[configSize++] = 0x00;
		cfg[configSize++] = 0x00;
		cfg[configSize++] = 0x01;

		configSize += vlc_b64_decode_binary_to_buffer(cfg + configSize,
			configMax - configSize, psz);
		psz += strlen(psz) + 1;
	}

	free(dup);
	return cfg;
}

/* Base64 decoding */
size_t vlc_b64_decode_binary_to_buffer(uint8_t *p_dst, size_t i_dst, const char *p_src)
{
	static const int b64[256] = {
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  /* 00-0F */
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  /* 10-1F */
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63,  /* 20-2F */
		52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1,  /* 30-3F */
		-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,  /* 40-4F */
		15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1,  /* 50-5F */
		-1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,  /* 60-6F */
		41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1,  /* 70-7F */
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  /* 80-8F */
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  /* 90-9F */
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  /* A0-AF */
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  /* B0-BF */
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  /* C0-CF */
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  /* D0-DF */
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  /* E0-EF */
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1   /* F0-FF */
	};
	uint8_t *p_start = p_dst;
	uint8_t *p = (uint8_t *)p_src;

	int i_level;
	int i_last;

	for (i_level = 0, i_last = 0; (size_t)(p_dst - p_start) < i_dst && *p != '\0'; p++)
	{
		const int c = b64[(unsigned int)*p];
		if (c == -1)
			break;

		switch (i_level)
		{
		case 0:
			i_level++;
			break;
		case 1:
			*p_dst++ = (i_last << 2) | ((c >> 4) & 0x03);
			i_level++;
			break;
		case 2:
			*p_dst++ = ((i_last << 4) & 0xf0) | ((c >> 2) & 0x0f);
			i_level++;
			break;
		case 3:
			*p_dst++ = ((i_last & 0x03) << 6) | c;
			i_level = 0;
		}
		i_last = c;
	}

	return p_dst - p_start;
}