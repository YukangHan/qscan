//FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   QScanRTSPClient.cpp
//
// PROJECT:     QScanItl
//
// Wrapper for rtsp client state's variables
// 
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//		YH		Yukang Han
//
/////////////////////////////////////////////
//
// Copyright (c) 2018 Innovent Technology Limited.
// All rigths reserved.
// 
// This software is protected by national and international copyright and
// other laws. Unauthorised use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
// 
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Innovent Technology Limited.
// 
///////////////////////////////////////////////////////////////////////////////

#include "QScanStreamClientState.h"

// Implementation of "StreamClientState":

StreamClientState::StreamClientState()
	: iter(NULL), session(NULL), subsession(NULL), streamTimerTask(NULL), duration(0.0), bSuccess(false)
{
	
}
//////////////////////////////////////////////////////////////////////////
StreamClientState::~StreamClientState()
{
	delete iter;
	if (session != NULL)
	{
		// We also need to delete "session", and unschedule "streamTimerTask" (if set)
		UsageEnvironment& env = session->envir(); // alias

		env.taskScheduler().unscheduleDelayedTask(streamTimerTask);
		Medium::close(session);
	}
}