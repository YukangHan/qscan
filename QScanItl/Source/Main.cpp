//FILE HEADER//////////////////////////////////////////////////////////////////
//
// FILE NAME:   Main.cpp
//
// PROJECT:     QScanItl
//
// Main QScanItl file
// 
/////////////////////////////////////////////
//
// CONTRIBUTORS:
//		YH		Yukang Han
//
/////////////////////////////////////////////
//
// Copyright (c) 2018 Innovent Technology Limited.
// All rigths reserved.
// 
// This software is protected by national and international copyright and
// other laws. Unauthorised use, storage, reproduction, transmission
// and/or distribution of this software, or any part of it, may result in
// civil and/or criminal proceedings.
// 
// This software is confidential and should not be disclosed, in whole or
// in part, to any person without the prior written permission of
// Innovent Technology Limited.
// 
///////////////////////////////////////////////////////////////////////////////

#include "RTSPClient.hh"

#include "QScanRTSPClient.h"
#include "QScanStreamClientState.h"
#include "QScanDummySink.h"
#include "QScanRTSPCallback.h"
#include "QScanCommonDefine.h"
#include "QScanApplication.h"

char eventLoopWatchVariable = 0;
Authenticator* ourAuthenticator = NULL;

int main(int argc, char** argv)
{
	if (!QScanApplication::get().Initialize(argc, argv))
		return false;

	if (!QScanApplication::get().OpenURL())
		return false;

	if (!QScanApplication::get().OpenWindow())
		return false;

	if (!QScanApplication::get().Run())
		return false;

	return 0;

  // If you choose to continue the application past this point (i.e., if you comment out the "return 0;" statement above),
  // and if you don't intend to do anything more with the "TaskScheduler" and "UsageEnvironment" objects,
  // then you can also reclaim the (small) memory used by these objects by uncommenting the following code:
  /*
    env->reclaim(); env = NULL;
    delete scheduler; scheduler = NULL;
  */
}