#ifndef _QSCANHALCONINTERFACE_H_
#define _QSCANHALCONINTERFACE_H_

//////////////////////////////////////////////////////////////////////////
#if defined(_WIN32) || defined(__WIN32__)
#	ifdef QSCANHALCON_EXPORTS
#	define QSCANHALCON_API extern "C" __declspec(dllexport)
#	else
#	define QSCANHALCON_API extern "C" __declspec(dllimport)
#	endif
#else 
#	if defined(linux) || defined(__linux)
#define QSCANHALCON_API
#	endif
#endif

//////////////////////////////////////////////////////////////////////////
QSCANHALCON_API void newHalcon();
//////////////////////////////////////////////////////////////////////////
QSCANHALCON_API void delHalcon();
//////////////////////////////////////////////////////////////////////////
QSCANHALCON_API void initHalcon();
//////////////////////////////////////////////////////////////////////////
QSCANHALCON_API void revHalcon(const unsigned int nWidth, const unsigned int nHeight, unsigned char* pBuffer, const unsigned int nBufferSize);
//////////////////////////////////////////////////////////////////////////

#endif
