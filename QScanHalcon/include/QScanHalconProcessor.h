#ifndef _HALCONPROCESSOR_H_
#define _HALCONPROCESSOR_H_

#ifndef __APPLE__
#  include "HalconCpp.h"
#  include "HDevThread.h"
#else
#  ifndef HC_LARGE_IMAGES
#    include <HALCONCpp/HalconCpp.h>
#    include <HALCONCpp/HDevThread.h>
#  else
#    include <HALCONCppxl/HalconCpp.h>
#    include <HALCONCppxl/HDevThread.h>
#  endif
#  include <stdio.h>
#  include <HALCON/HpThread.h>
#  include <CoreFoundation/CFRunLoop.h>
#endif

struct QScanHalconProcessor
{
	QScanHalconProcessor(class QScanHalcon& pQScanHalcon);
	
	~QScanHalconProcessor();

	void Process();

	bool ProcessFrame(const struct IN_FRAMEDATA* data);

	bool GenerateImage(const struct IN_FRAMEDATA* data);

	bool ProcessImage();

private:

	void Pre_Seg_Pig(HalconCpp::HObject ho_ImageZ1, HalconCpp::HObject *ho_Pigs1, HalconCpp::HTuple hv_Height1, HalconCpp::HTuple hv_Width1, HalconCpp::HTuple hv_PigCircleSize1);

	void select_shape_xld_std(HalconCpp::HObject ho_Contours2, HalconCpp::HObject *ho_LongestCont1);

	void find_pig_points(HalconCpp::HObject ho_PigHor1, HalconCpp::HTuple *hv_PigRearEndRef1, HalconCpp::HTuple *hv_PigShoulderStartRef1);

	void find_pig_ends(HalconCpp::HTuple hv_LocalMin, HalconCpp::HTuple hv_ArrayLength, HalconCpp::HTuple hv_StartPercent, HalconCpp::HTuple hv_EndPercent, HalconCpp::HTuple hv_ColumnRef, HalconCpp::HTuple *hv_PigEndIndex, HalconCpp::HTuple *hv_PigEndRef);

	void get_pig_width(HalconCpp::HObject ho_PigRotate, HalconCpp::HTuple *hv_WidthArrayLength, HalconCpp::HTuple *hv_PigRotateColumnRef, HalconCpp::HTuple *hv_PigWidth);

private:

	class QScanHalcon& m_pDataSource;

	class HalconCpp::HImage m_kImage;
};

#endif
