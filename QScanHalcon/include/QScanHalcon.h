#ifndef _QSCANHALCON_H_
#define _QSCANHALCON_H_

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the QSCANHALCON_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// QSCANHALCON_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.


#include "QScanHalconInterface.h"
#include "QScanHalconProcessor.h"
#include <list>
#include <thread>
#include <mutex>
#include <condition_variable>

//////////////////////////////////////////////////////////////////////////
struct IN_FRAMEDATA
{
	unsigned int		nImageWidth;
	unsigned int		nImageHeight;
	unsigned char*		pBuffer;
	unsigned int		nBufferSize;
};

// This class is exported from the Halcon.dll
class QScanHalcon
{

public:

	friend void QScanHalconProcessor::Process();

public:
	QScanHalcon(void);
	~QScanHalcon(void);
	// TODO: add your methods here.

	bool Initialize();

	void Update(unsigned int nDelta);
	void Render(unsigned int nDelta);

	void Receive(const unsigned int nWidth, const unsigned int nHeight, unsigned char* pBuffer, const unsigned int nBufferSize);

private:

	std::list<std::thread>	m_kHalconThreadList;

	std::mutex				m_kMutex;

	std::condition_variable m_kConditionVariable;

	std::list<IN_FRAMEDATA> m_kFrameBufferList;
};

#endif