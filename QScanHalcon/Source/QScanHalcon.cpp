// QScanHalcon.cpp : Defines the exported functions for the DLL application.
//

#include "QScanHalcon.h"
#include <algorithm>
#include <thread>

//////////////////////////////////////////////////////////////////////////
QScanHalcon* g_qscan_halcon_interface = nullptr;
//////////////////////////////////////////////////////////////////////////
QScanHalcon::QScanHalcon()
{
	
}
//////////////////////////////////////////////////////////////////////////
QScanHalcon::~QScanHalcon()
{
	
}
//////////////////////////////////////////////////////////////////////////
bool QScanHalcon::Initialize()
{
	try
	{
		HalconCpp::SetSystem("parallelize_operators", "false");
	}
	catch (HalconCpp::HException &except)
	{
		Herror error_num = except.ErrorCode();

		HalconCpp::HString err = except.ErrorMessage();

		const char* strerr = err.Text();
	}

	unsigned long const default_threads_num = 2;
	unsigned long const hardware_threads = std::thread::hardware_concurrency();
	unsigned long const num_threads = hardware_threads != 0 ? hardware_threads : default_threads_num;

	for (unsigned int nIndex = 0; nIndex < num_threads; ++nIndex)
	{
		QScanHalconProcessor* processor = new QScanHalconProcessor(*this);
		m_kHalconThreadList.push_back(std::thread(&QScanHalconProcessor::Process, processor));
	}

	for (std::list<std::thread>::iterator iter = m_kHalconThreadList.begin(); iter != m_kHalconThreadList.end(); ++iter)
	{
		iter->detach();
	}

	return true;
}
//////////////////////////////////////////////////////////////////////////
void QScanHalcon::Update(unsigned int nDelta)
{

}
//////////////////////////////////////////////////////////////////////////
void QScanHalcon::Render(unsigned int nDelta)
{

}
//////////////////////////////////////////////////////////////////////////
void QScanHalcon::Receive(const unsigned int nWidth, const unsigned int nHeight, unsigned char* pBuffer, const unsigned int nBufferSize)
{
	std::lock_guard<std::mutex> lk(m_kMutex);

	unsigned char*  pHalconRGB = new unsigned char[nBufferSize];

	if (!pHalconRGB)
		return;

	memcpy(pHalconRGB, pBuffer, nBufferSize);

	m_kFrameBufferList.push_back(IN_FRAMEDATA{ nWidth, nHeight, pHalconRGB, nBufferSize });

	m_kConditionVariable.notify_one();
}
//////////////////////////////////////////////////////////////////////////
QSCANHALCON_API void newHalcon()
{
	if (g_qscan_halcon_interface == nullptr)
	{
		g_qscan_halcon_interface = new QScanHalcon();
	}
}
//////////////////////////////////////////////////////////////////////////
QSCANHALCON_API void delHalcon()
{
	if (g_qscan_halcon_interface != nullptr)
	{
		delete g_qscan_halcon_interface;
		g_qscan_halcon_interface = nullptr;
	}
}
//////////////////////////////////////////////////////////////////////////
QSCANHALCON_API void initHalcon()
{
	if (g_qscan_halcon_interface != nullptr)
	{
		g_qscan_halcon_interface->Initialize();
	}
}
//////////////////////////////////////////////////////////////////////////
QSCANHALCON_API void revHalcon(const unsigned int nWidth, const unsigned int nHeight, unsigned char* pBuffer, const unsigned int nBufferSize)
{
	if (g_qscan_halcon_interface != nullptr)
	{
		g_qscan_halcon_interface->Receive(nWidth, nHeight, pBuffer, nBufferSize);
	}
}