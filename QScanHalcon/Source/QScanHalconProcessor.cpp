#include "QScanHalconProcessor.h"
#include "QScanHalcon.h"
#include <mutex>
#include <condition_variable>
#include <chrono>

//////////////////////////////////////////////////////////////////////////
QScanHalconProcessor::QScanHalconProcessor(QScanHalcon& pQScanHalcon)
:m_pDataSource(pQScanHalcon)
{

}
//////////////////////////////////////////////////////////////////////////
QScanHalconProcessor::~QScanHalconProcessor()
{

}
//////////////////////////////////////////////////////////////////////////
void QScanHalconProcessor::Process()
{
 	while (true)
 	{
		std::unique_lock<std::mutex> lk(m_pDataSource.m_kMutex);

		m_pDataSource.m_kConditionVariable.wait(lk, [&]{return !m_pDataSource.m_kFrameBufferList.empty(); });
			
		IN_FRAMEDATA data = m_pDataSource.m_kFrameBufferList.front();

		m_pDataSource.m_kFrameBufferList.pop_front();

		lk.unlock();

		ProcessFrame(&data);

		if (data.pBuffer != nullptr)
		{
			delete[] data.pBuffer;
			data.pBuffer = nullptr;
		}
 	}
}
//////////////////////////////////////////////////////////////////////////
bool QScanHalconProcessor::ProcessFrame(const IN_FRAMEDATA* data)
{
	auto start = std::chrono::high_resolution_clock::now();

	if (!GenerateImage(data))
		return false;

	ProcessImage();

	auto stop = std::chrono::high_resolution_clock::now();

	std::cout << "ProcessFrame() took " << std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count() << " milliseconds" << std::endl;

	return true;
}
//////////////////////////////////////////////////////////////////////////
bool QScanHalconProcessor::GenerateImage(const struct IN_FRAMEDATA* data)
{
	using namespace HalconCpp;

	Herror  error_num;

	try
	{
		m_kImage.GenImage1("byte", data->nImageWidth, data->nImageHeight, data->pBuffer);
		//m_kImage.GenImageInterleaved(data->pBuffer, "rgb", data->nImageWidth, data->nImageHeight, 0, "byte", 0, 0, 0, 0, -1, 0);
	}
	catch (HException &except)
	{
		error_num = except.ErrorCode();

		HString err = except.ErrorMessage();

		const char* strerr = err.Text();

		return error_num == 0;
	}

	return true;
}
//////////////////////////////////////////////////////////////////////////
bool QScanHalconProcessor::ProcessImage()
{
	using namespace HalconCpp;

	//Qiming's code______________________________________________________
	HObject  ho_Tmp, ho_PigsTmp;
	HObject  ho_PigSelected, ho_PigHor, ho_ImageHor, ho_Skeleton;
	HObject  ho_Contours, ho_LongestCont, ho_Region, ho_Parts;
	HObject  ho_Partitioned1, ho_Rear, ho_Mid, ho_Shoulder;

	// Local control variables
	HTuple  hv_DIR, hv_ImageFiles, hv_NumImg, hv_Width, hv_Tmp;
	HTuple  hv_Height, hv_IndexI;
	HTuple  hv_PigCircleSize, hv_Min, hv_Max, hv_Range, hv_Alpha;
	HTuple  hv_Beta, hv_Gamma, hv_Area, hv_Row, hv_Column, hv_PigNumber;
	HTuple  hv_IndexP, hv_Ra, hv_Rb, hv_Phi, hv_HomMat2DIdentity;
	HTuple  hv_HomMat2D, hv_RowBegin, hv_ColBegin, hv_RowEnd;
	HTuple  hv_ColEnd, hv_Nr, hv_Nc, hv_Dist, hv_Orientation;
	HTuple  hv_PhiDeg, hv_Anisometry, hv_Bulkiness, hv_StructureFactor;
	HTuple  hv_PigRearEndRef, hv_PigShoulderStartRef, hv_PartsNumber;
	HTuple  hv_Areas, hv_Rows, hv_Columns, hv_AreaRate;
	//______________________________________________________________________
	

	//
	// Do all the image processing - Qiming to fill in :-)
	//
	hv_Areas[0] = 0; hv_Areas[1] = 0; hv_Areas[2] = 0;
	hv_PigCircleSize = 15;
	try
	{
		//
		// Create an HImage version of the IfsImage
		//

		GetImageSize(m_kImage, &hv_Width, &hv_Height);
		Pre_Seg_Pig(m_kImage, &ho_PigsTmp, hv_Height, hv_Width, hv_PigCircleSize);
		CountObj(ho_PigsTmp, &hv_PigNumber);


		HTuple end_val29 = hv_PigNumber;
		HTuple step_val29 = 1;
		for (hv_IndexP = 1; hv_IndexP.Continue(end_val29, step_val29); hv_IndexP += step_val29)
		{
			SelectObj(ho_PigsTmp, &ho_PigSelected, hv_IndexP);
			EllipticAxis(ho_PigSelected, &hv_Ra, &hv_Rb, &hv_Phi);
			AreaCenter(ho_PigSelected, &hv_Area, &hv_Row, &hv_Column);

			HomMat2dIdentity(&hv_HomMat2DIdentity);
			VectorAngleToRigid(hv_Row, hv_Column, 0, hv_Height / 2, hv_Width / 2, -hv_Phi,
				&hv_HomMat2D);
			AffineTransRegion(ho_PigSelected, &ho_PigHor, hv_HomMat2D, "nearest_neighbor");
			//CopyObj(tmpImg, &ho_ImageHor, 1, 1);
			AffineTransImage(m_kImage, &m_kImage, hv_HomMat2D, "weighted", "false");
			//
			//Check Pig Bends
			Skeleton(ho_PigHor, &ho_Skeleton);
			GenContoursSkeletonXld(ho_Skeleton, &ho_Contours, 1, "filter");
			select_shape_xld_std(ho_Contours, &ho_LongestCont);
			FitLineContourXld(ho_LongestCont, "regression", -1, 0, 5, 2, &hv_RowBegin,
				&hv_ColBegin, &hv_RowEnd, &hv_ColEnd, &hv_Nr, &hv_Nc, &hv_Dist);
			LineOrientation(hv_RowBegin, hv_ColBegin, hv_RowEnd, hv_ColEnd, &hv_Orientation);
			hv_PhiDeg = hv_Orientation.TupleDeg();
			if (0 != ((hv_PhiDeg.TupleAbs()) > 5))
			{
				continue;
			}
			GenRegionContourXld(ho_LongestCont, &ho_Region, "filled");
			Eccentricity(ho_Region, &hv_Anisometry, &hv_Bulkiness, &hv_StructureFactor);
			//if (Anisometry < 10)
			//continue
			//endif
			//
			//Detect Narrow points
			find_pig_points(ho_PigHor, &hv_PigRearEndRef, &hv_PigShoulderStartRef);
			GenEmptyObj(&ho_Parts);
			if (0 != (HTuple(hv_PigShoulderStartRef > hv_PigRearEndRef).TupleAnd(hv_PigRearEndRef > 0)))
			{
				PartitionRectangle(ho_PigHor, &ho_Partitioned1, 1, 500);
				SelectShape(ho_Partitioned1, &ho_Rear, "column1", "and", 0, hv_PigRearEndRef);
				Union1(ho_Rear, &ho_Rear);
				ConcatObj(ho_Parts, ho_Rear, &ho_Parts);
				SelectShape(ho_Partitioned1, &ho_Mid, "column1", "and", hv_PigRearEndRef + 1,
					hv_PigShoulderStartRef);
				Union1(ho_Mid, &ho_Mid);
				ConcatObj(ho_Parts, ho_Mid, &ho_Parts);
				SelectShape(ho_Partitioned1, &ho_Shoulder, "column1", "and", hv_PigShoulderStartRef + 1,
					9999);
				Union1(ho_Shoulder, &ho_Shoulder);
				ConcatObj(ho_Parts, ho_Shoulder, &ho_Parts);
			}
			CountObj(ho_Parts, &hv_PartsNumber);
			if (0 != (hv_PartsNumber == 0))
			{
				continue;
			}
			AreaCenter(ho_Parts, &hv_Areas, &hv_Rows, &hv_Columns);
			//Paint Color onto the pig parts
			GenContourRegionXld(ho_Parts, &ho_Contours, "border");
			for (hv_IndexI = 1; hv_IndexI <= hv_PartsNumber; hv_IndexI += 1)
				//GenContourRegionXld(ho_PigSelected, &ho_Contours, "border");
				//for (hv_IndexI = 1; hv_IndexI <= hv_PigNumber; hv_IndexI += 1)
			{
				SelectObj(ho_Contours, &ho_Tmp, hv_IndexI);
				GetContourXld(ho_Tmp, &hv_Rows, &hv_Columns);
				TupleGenConst(hv_Rows.TupleLength(), 255, &hv_Tmp);
				SetGrayval(m_kImage, hv_Rows, hv_Columns, hv_Tmp);
			}
			//WriteImage(ho_ImageHor, "jpeg", 0, "C:/Users/dol-sensors/Desktop/234.jpg");
			//WriteImage(tmpImg, "jpeg", 0, "C:/Users/dol-sensors/Desktop/235.jpg");

			//hv_AreaRate = (HTuple(hv_Areas[0]) * 100) / HTuple(hv_Areas[2]);
			/*if (0 != (HTuple(hv_AreaRate > 150).TupleOr(hv_AreaRate < 67)))
			{
			continue;
			}*/

		}
	}
	catch (...)
	{

	}


	m_kImage.InvertImage();


	//
	// Calculate conversion factor from pels to mm
	//
	Hlong area1, area2, area3;

	// Fill in the result information 
	//
	area1 = hv_Areas[0];
	area2 = hv_Areas[1];
	area3 = hv_Areas[2];

	/*oinfo->xswm = 0.0f;
	oinfo->yswm = 0.0f;
	oinfo->comp = 9999.99f;*/

	return 0;
}
//////////////////////////////////////////////////////////////////////////
void QScanHalconProcessor::Pre_Seg_Pig(HalconCpp::HObject ho_ImageZ1, HalconCpp::HObject *ho_Pigs1, HalconCpp::HTuple hv_Height1, HalconCpp::HTuple hv_Width1, HalconCpp::HTuple hv_PigCircleSize1)
{
	using namespace HalconCpp;

	// Local iconic variables
	HObject  ho_SelectedRegions, ho_DerivGauss, ho_RegionHysteresis;
	HObject  ho_Rect, ho_Frame, ho_RegionUnion, ho_Skeleton1;
	HObject  ho_RegionDifference, ho_RegionFillUp, ho_RegionOpening;
	HObject  ho_PigsTmp1, ho_ImageCED1;
	HTuple  hv_Ra, hv_Rb, hv_Phi, hv_exception, hv_tmp;

	try
	{
		//
		//Pig segmentation
		GenEmptyObj(&(*ho_Pigs1));
		GenEmptyObj(&ho_DerivGauss);
		//Process gray values -> Emphasize contrast and directional smoothing
		CoherenceEnhancingDiff(ho_ImageZ1, &ho_ImageCED1, 1, 1, 0.5, 3);
		//
		//Pig separation
		DerivateGauss(ho_ImageCED1, &ho_DerivGauss, 1.732, (HTuple("area")));
		DualThreshold(ho_DerivGauss, &ho_RegionHysteresis, 200, 10, 6);
		//
		GenRectangle1(&ho_Rect, 1, 1, hv_Height1 - 2, hv_Width1 - 2);
		Difference(ho_ImageZ1, ho_Rect, &ho_Frame);
		Union2(ho_RegionHysteresis, ho_Frame, &ho_RegionUnion);
		Skeleton(ho_RegionUnion, &ho_Skeleton1);
		Threshold(ho_ImageZ1, &ho_SelectedRegions, 0, 255);
		Difference(ho_SelectedRegions, ho_Skeleton1, &ho_RegionDifference);
		//
		OpeningCircle(ho_RegionDifference, &ho_RegionOpening, hv_PigCircleSize1);
		SetSystem("neighborhood", 4);
		Connection(ho_RegionOpening, &ho_PigsTmp1);
		DilationCircle(ho_PigsTmp1, &ho_PigsTmp1, 1);
		SelectShape(ho_PigsTmp1, &ho_PigsTmp1, (((HTuple("anisometry").Append("rectangularity")).Append("circularity")).Append("area")),
			"and", ((HTuple(2.0).Append(0.75)).Append(0.2)).TupleConcat((0.05*hv_Height1)*hv_Width1),
			(((HTuple(3.7).Append(0.95)).Append(0.45)).Append(99999)));
		CountObj(ho_PigsTmp1, &hv_tmp);
		if (hv_tmp > 0)
		{
			EllipticAxis(ho_PigsTmp1, &hv_Ra, &hv_Rb, &hv_Phi);
			OpeningCircle(ho_PigsTmp1, &ho_PigsTmp1, ((hv_Rb.TupleMean()) / 4) * 3);
		}
		SelectShape(ho_PigsTmp1, &ho_PigsTmp1, ((HTuple("anisometry").Append("rectangularity")).Append("circularity")),
			"and", ((HTuple(2.0).Append(0.8)).Append(0.35)), ((HTuple(3.3).Append(0.95)).Append(0.45)));
		SelectShape(ho_PigsTmp1, &ho_PigsTmp1, (((HTuple("row1").Append("column1")).Append("row2")).Append("column2")),
			"and", (((HTuple(10).Append(10)).Append(10)).Append(10)), (((hv_Height1 - 10).TupleConcat(hv_Width1 - 10)).TupleConcat(hv_Height1 - 10)).TupleConcat(hv_Width1 - 10));
		SelectShape(ho_PigsTmp1, &(*ho_Pigs1), (HTuple("row").Append("column")), "and", (hv_Height1*.1).TupleConcat(hv_Width1*.1),
			(hv_Height1*.9).TupleConcat(hv_Width1*.9));

		return;
	}
	catch (HalconCpp::HException &HDevExpDefaultException)
	{
		HString err = HDevExpDefaultException.ErrorMessage();

		const char* strerr = err.Text();

		return;
	}
}
//////////////////////////////////////////////////////////////////////////
void QScanHalconProcessor::select_shape_xld_std(HalconCpp::HObject ho_Contours2, HalconCpp::HObject *ho_LongestCont1)
{
	using namespace HalconCpp;
	// Local iconic variables
	HObject  ho_Region1, ho_Contours1;

	// Local control variables
	HTuple  hv_Length, hv_IndicesL, hv_LongestIndex;

	GenRegionContourXld(ho_Contours2, &ho_Region1, "filled");
	GenContourRegionXld(ho_Region1, &ho_Contours1, "border");
	LengthXld(ho_Contours1, &hv_Length);
	TupleSortIndex(hv_Length, &hv_IndicesL);
	hv_LongestIndex = HTuple(hv_IndicesL[(hv_IndicesL.TupleLength()) - 1]) + 1;
	SelectObj(ho_Contours2, &(*ho_LongestCont1), hv_LongestIndex);
	return;
}
//////////////////////////////////////////////////////////////////////////
void QScanHalconProcessor::find_pig_points(HalconCpp::HObject ho_PigHor1, HalconCpp::HTuple *hv_PigRearEndRef1, HalconCpp::HTuple *hv_PigShoulderStartRef1)
{
	using namespace HalconCpp;

	// Local iconic variables
	HObject  ho_PigPart, ho_Top, ho_Bot, ho_PigRotate;
	HObject  ho_TopContours, ho_BotContours;

	// Local control variables
	HTuple  hv_Row1, hv_Column1, hv_Row2, hv_Column2;
	HTuple  hv_WidthP, hv_HeightP, hv_PigArrayLength, hv_PigRotateColumnRef;
	HTuple  hv_PigWidth, hv_TopArrayLength, hv_TopRotateColumnRef;
	HTuple  hv_TopWidth, hv_BotArrayLength, hv_BotRotateColumnRef;
	HTuple  hv_BotWidth, hv_TopFunction, hv_BotFunction, hv_Function;
	HTuple  hv_TopSmoothedFunction, hv_BotSmoothedFunction;
	HTuple  hv_SmoothedFunction, hv_TopMin, hv_TopMax, hv_BotMin;
	HTuple  hv_BotMax, hv_PigMin, hv_PigMax, hv_PigMidWidth;
	HTuple  hv_PigMidIndex, hv_i, hv_PigRearEndIndex, hv_TopRearEndIndex;
	HTuple  hv_BotRearEndIndex, hv_TopRearEndRef, hv_BotRearEndRef;
	HTuple  hv_PigShoulderStartIndex, hv_TopShoulderStartIndex;
	HTuple  hv_BotShoulderStartIndex, hv_TopShoulderStartRef;
	HTuple  hv_BotShoulderStartRef;

	SmallestRectangle1(ho_PigHor1, &hv_Row1, &hv_Column1, &hv_Row2, &hv_Column2);
	hv_WidthP = hv_Column2 - hv_Column1;
	hv_HeightP = hv_Row2 - hv_Row1;
	PartitionRectangle(ho_PigHor1, &ho_PigPart, hv_WidthP, hv_HeightP / 2);
	SelectObj(ho_PigPart, &ho_Top, 1);
	SelectObj(ho_PigPart, &ho_Bot, 2);
	GenContourRegionXld(ho_PigHor1, &ho_PigRotate, "border");
	GenContourRegionXld(ho_Top, &ho_TopContours, "border");
	GenContourRegionXld(ho_Bot, &ho_BotContours, "border");
	get_pig_width(ho_PigRotate, &hv_PigArrayLength, &hv_PigRotateColumnRef, &hv_PigWidth);
	get_pig_width(ho_TopContours, &hv_TopArrayLength, &hv_TopRotateColumnRef, &hv_TopWidth);
	get_pig_width(ho_BotContours, &hv_BotArrayLength, &hv_BotRotateColumnRef, &hv_BotWidth);
	//
	CreateFunct1dArray(hv_TopWidth, &hv_TopFunction);
	CreateFunct1dArray(hv_BotWidth, &hv_BotFunction);
	CreateFunct1dArray(hv_PigWidth, &hv_Function);
	//
	SmoothFunct1dGauss(hv_TopFunction, 5, &hv_TopSmoothedFunction);
	SmoothFunct1dGauss(hv_BotFunction, 5, &hv_BotSmoothedFunction);
	SmoothFunct1dGauss(hv_Function, 5, &hv_SmoothedFunction);
	//
	LocalMinMaxFunct1d(hv_TopSmoothedFunction, "strict_min_max", "true", &hv_TopMin,
		&hv_TopMax);
	LocalMinMaxFunct1d(hv_BotSmoothedFunction, "strict_min_max", "true", &hv_BotMin,
		&hv_BotMax);
	LocalMinMaxFunct1d(hv_SmoothedFunction, "plateaus_center", "true", &hv_PigMin,
		&hv_PigMax);
	//
	//
	//* Mid Belly Profile Points
	hv_PigMidWidth = 0;
	hv_PigMidIndex = 0;
	{
		HTuple end_val29 = (3 * hv_PigArrayLength) / 5;
		HTuple step_val29 = 1;
		for (hv_i = (2 * hv_PigArrayLength) / 5; hv_i.Continue(end_val29, step_val29); hv_i += step_val29)
		{
			if (0 != (HTuple(hv_PigWidth[hv_i]) > hv_PigMidWidth))
			{
				//
				hv_PigMidWidth = ((const HTuple&)hv_PigWidth)[hv_i];
				hv_PigMidIndex = hv_i;
				//
			}
		}
	}
	//
	//* Rear End Points **
	hv_PigRearEndIndex = 0;
	hv_TopRearEndIndex = 0;
	hv_BotRearEndIndex = 0;
	(*hv_PigRearEndRef1) = 0;
	hv_TopRearEndRef = 0;
	hv_BotRearEndRef = 0;
	find_pig_ends(hv_TopMin, hv_TopArrayLength, 25, 43, hv_TopRotateColumnRef, &hv_TopRearEndIndex,
		&hv_TopRearEndRef);
	find_pig_ends(hv_BotMin, hv_BotArrayLength, 25, 43, hv_BotRotateColumnRef, &hv_BotRearEndIndex,
		&hv_BotRearEndRef);
	find_pig_ends(hv_PigMin, hv_PigArrayLength, 25, 43, hv_PigRotateColumnRef, &hv_PigRearEndIndex,
		&(*hv_PigRearEndRef1));
	//
	//* Compare Top and Bot Rear End
	if (0 != (HTuple(hv_TopRearEndIndex > 0).TupleAnd(hv_BotRearEndIndex > 0)))
	{
		hv_TopRearEndRef = ((const HTuple&)hv_TopRotateColumnRef)[hv_TopRearEndIndex];
		hv_BotRearEndRef = ((const HTuple&)hv_BotRotateColumnRef)[hv_BotRearEndIndex];
		(*hv_PigRearEndRef1) = (hv_TopRearEndRef + hv_BotRearEndRef) / 2;
		hv_PigRearEndIndex = (hv_TopRearEndIndex + hv_BotRearEndIndex) / 2;
	}
	if (0 != (HTuple(hv_TopRearEndIndex > 0).TupleAnd(hv_BotRearEndIndex < 1)))
	{
		hv_PigRearEndIndex = hv_TopRearEndIndex;
		(*hv_PigRearEndRef1) = ((const HTuple&)hv_TopRotateColumnRef)[hv_TopRearEndIndex];
	}
	if (0 != (HTuple(hv_TopRearEndIndex < 1).TupleAnd(hv_BotRearEndIndex>0)))
	{
		hv_PigRearEndIndex = hv_BotRearEndIndex;
		(*hv_PigRearEndRef1) = ((const HTuple&)hv_BotRotateColumnRef)[hv_BotRearEndIndex];
	}
	//
	//* Shoulder Start Points**
	hv_PigShoulderStartIndex = 0;
	hv_TopShoulderStartIndex = 0;
	hv_BotShoulderStartIndex = 0;
	(*hv_PigShoulderStartRef1) = 0;
	hv_TopShoulderStartRef = 0;
	hv_BotShoulderStartRef = 0;
	find_pig_ends(hv_TopMin, hv_TopArrayLength, 60, 75, hv_TopRotateColumnRef, &hv_TopShoulderStartIndex,
		&hv_TopShoulderStartRef);
	find_pig_ends(hv_BotMin, hv_BotArrayLength, 60, 75, hv_BotRotateColumnRef, &hv_BotShoulderStartIndex,
		&hv_BotShoulderStartRef);
	find_pig_ends(hv_PigMin, hv_PigArrayLength, 60, 75, hv_PigRotateColumnRef, &hv_PigShoulderStartIndex,
		&(*hv_PigShoulderStartRef1));
	//* Compare Top and Bot Shoulder Start
	if (0 != (HTuple(hv_TopShoulderStartIndex > 0).TupleAnd(hv_BotShoulderStartIndex > 0)))
	{
		hv_TopShoulderStartRef = ((const HTuple&)hv_TopRotateColumnRef)[hv_TopShoulderStartIndex];
		hv_BotShoulderStartRef = ((const HTuple&)hv_BotRotateColumnRef)[hv_BotShoulderStartIndex];
		(*hv_PigShoulderStartRef1) = (hv_TopShoulderStartRef + hv_BotShoulderStartRef) / 2;
		hv_PigShoulderStartIndex = (hv_TopShoulderStartIndex + hv_BotShoulderStartIndex) / 2;
	}
	if (0 != (HTuple(hv_TopShoulderStartIndex > 0).TupleAnd(hv_BotShoulderStartIndex < 1)))
	{
		hv_PigShoulderStartIndex = hv_TopShoulderStartIndex;
		(*hv_PigShoulderStartRef1) = ((const HTuple&)hv_TopRotateColumnRef)[hv_TopShoulderStartIndex];
	}
	if (0 != (HTuple(hv_TopShoulderStartIndex < 1).TupleAnd(hv_BotShoulderStartIndex>0)))
	{
		hv_PigShoulderStartIndex = hv_BotShoulderStartIndex;
		(*hv_PigShoulderStartRef1) = ((const HTuple&)hv_BotRotateColumnRef)[hv_BotShoulderStartIndex];
	}
	return;
}
//////////////////////////////////////////////////////////////////////////
void QScanHalconProcessor::find_pig_ends(HalconCpp::HTuple hv_LocalMin, HalconCpp::HTuple hv_ArrayLength, HalconCpp::HTuple hv_StartPercent, HalconCpp::HTuple hv_EndPercent, HalconCpp::HTuple hv_ColumnRef, HalconCpp::HTuple *hv_PigEndIndex, HalconCpp::HTuple *hv_PigEndRef)
{
	using namespace HalconCpp;

	// Local iconic variables

	// Local control variables
	HTuple  hv_PigEndsFlag, hv_Min, hv_i;

	(*hv_PigEndIndex) = 0;
	hv_PigEndsFlag = 0;
	hv_Min = hv_LocalMin;
	//
	{
		HTuple end_val4 = (hv_LocalMin.TupleLength()) - 1;
		HTuple step_val4 = 1;
		for (hv_i = 0; hv_i.Continue(end_val4, step_val4); hv_i += step_val4)
		{
			if (0 != (HTuple(HTuple(HTuple(hv_LocalMin[hv_i]) >= ((hv_StartPercent*hv_ArrayLength) / 100)).TupleAnd(HTuple(hv_LocalMin[hv_i]) <= ((hv_EndPercent*hv_ArrayLength) / 100))).TupleAnd(hv_PigEndsFlag == 0)))
			{
				(*hv_PigEndIndex) = HTuple(hv_LocalMin[hv_i]).TupleRound();
				(*hv_PigEndRef) = ((const HTuple&)hv_ColumnRef)[(*hv_PigEndIndex)];
				hv_PigEndsFlag = 1;
			}
		}
	}
	//
	return;
}
//////////////////////////////////////////////////////////////////////////
void QScanHalconProcessor::get_pig_width(HalconCpp::HObject ho_PigRotate, HalconCpp::HTuple *hv_WidthArrayLength, HalconCpp::HTuple *hv_PigRotateColumnRef, HalconCpp::HTuple *hv_PigWidth)
{
	using namespace HalconCpp;

	// Local iconic variables

	// Local control variables
	HTuple  hv_Length, hv_PigRotateRows, hv_PigRotateColumns;
	HTuple  hv_Ra, hv_Rb, hv_Phi, hv_Area, hv_PigCentreRow;
	HTuple  hv_PigCentreColumn, hv_PointOrder, hv_CheckingDistance;
	HTuple  hv_MouthColumnPoint, hv_TailColumnPoint, hv_MouthColumnIndex;
	HTuple  hv_TailColumnIndex, hv_i, hv_NextPoint, hv_FivePoint;
	HTuple  hv_FiveBack, hv_FishLengthPixels, hv_LeftWidthtoCentre;
	HTuple  hv_RightWidthtoCentre, hv_ColumnRef;

	LengthXld(ho_PigRotate, &hv_Length);
	TupleMax(hv_Length, &hv_Length);
	SelectContoursXld(ho_PigRotate, &ho_PigRotate, "contour_length", hv_Length - 10,
		hv_Length + 10, -0.5, 0.5);
	GetContourXld(ho_PigRotate, &hv_PigRotateRows, &hv_PigRotateColumns);
	//Create Polygon of Pig
	//gen_polygons_xld (PigRotate, Polygons, 'ramer', 0.01)
	//get_polygon_xld (Polygons, PolyRow, PolyCol, PolyLength, PolyPhi)
	EllipticAxisPointsXld(ho_PigRotate, &hv_Ra, &hv_Rb, &hv_Phi);
	AreaCenterXld(ho_PigRotate, &hv_Area, &hv_PigCentreRow, &hv_PigCentreColumn, &hv_PointOrder);
	//Length of Pig to Check for Fin
	hv_CheckingDistance = hv_Ra / 9;
	TupleRound(hv_CheckingDistance, &hv_CheckingDistance);
	//
	//Initialise Values
	hv_MouthColumnPoint = 0;
	hv_TailColumnPoint = 0;
	hv_MouthColumnIndex = 0;
	hv_TailColumnIndex = 0;
	//
	//Check Each Point of Pig Contour
	{
		HTuple end_val20 = (hv_PigRotateColumns.TupleLength()) - 2;
		HTuple step_val20 = 1;
		for (hv_i = 1; hv_i.Continue(end_val20, step_val20); hv_i += step_val20)
		{
			//* Increment Wrap Around
			hv_NextPoint = hv_i + 1;
			if (0 != (hv_NextPoint > ((hv_PigRotateColumns.TupleLength()) - 1)))
			{
				hv_NextPoint = (hv_i + 2) - (hv_PigRotateColumns.TupleLength());
			}
			hv_FivePoint = hv_i + hv_CheckingDistance;
			if (0 != (hv_FivePoint > ((hv_PigRotateColumns.TupleLength()) - 1)))
			{
				hv_FivePoint = ((hv_i + hv_CheckingDistance) + 1) - (hv_PigRotateColumns.TupleLength());
			}
			hv_FiveBack = hv_i - hv_CheckingDistance;
			if (0 != (hv_FiveBack < 0))
			{
				hv_FiveBack = (hv_i - hv_CheckingDistance) + (hv_PigRotateColumns.TupleLength());
			}
			//
			//Find Mouth and Tail Point Cross Principal Axis
			if (0 != (HTuple(HTuple(HTuple(hv_PigRotateRows[hv_NextPoint]) < hv_PigCentreRow).TupleAnd(HTuple(hv_PigRotateRows[hv_i - 1]) > hv_PigCentreRow)).TupleOr(HTuple(HTuple(hv_PigRotateRows[hv_NextPoint]) > hv_PigCentreRow).TupleAnd(HTuple(hv_PigRotateRows[hv_i - 1]) < hv_PigCentreRow))))
			{
				if (0 != (HTuple(hv_PigRotateColumns[hv_i]) > hv_PigCentreColumn))
				{
					hv_TailColumnIndex = hv_i;
					hv_TailColumnPoint = ((const HTuple&)hv_PigRotateColumns)[hv_i];
				}
				//
				if (0 != (HTuple(hv_PigRotateColumns[hv_i]) < hv_PigCentreColumn))
				{
					hv_MouthColumnIndex = hv_i;
					hv_MouthColumnPoint = ((const HTuple&)hv_PigRotateColumns)[hv_i];
				}
			}
			//
		}
	}
	//
	//
	if (0 != (HTuple(hv_MouthColumnIndex != 0).TupleAnd(hv_TailColumnIndex != 0)))
	{
		//
		hv_FishLengthPixels = hv_TailColumnPoint - hv_MouthColumnPoint;
		TupleRound(hv_FishLengthPixels, &hv_FishLengthPixels);
		hv_LeftWidthtoCentre = 0;
		hv_RightWidthtoCentre = 0;
		hv_LeftWidthtoCentre[hv_FishLengthPixels + 1] = 0;
		hv_RightWidthtoCentre[hv_FishLengthPixels + 1] = 0;
		//
		//* Find Width of either side of pig
		{
			HTuple end_val61 = (hv_PigRotateColumns.TupleLength()) - 1;
			HTuple step_val61 = 1;
			for (hv_i = 0; hv_i.Continue(end_val61, step_val61); hv_i += step_val61)
			{
				hv_ColumnRef = HTuple(hv_PigRotateColumns[hv_i]) - hv_MouthColumnPoint;
				TupleRound(hv_ColumnRef, &hv_ColumnRef);
				//
				if (0 != (hv_ColumnRef < 0))
				{
					hv_ColumnRef = 0;
				}
				//
				if (0 != (HTuple(hv_PigRotateRows[hv_i]) > hv_PigCentreRow))
				{
					hv_LeftWidthtoCentre[hv_ColumnRef] = HTuple(hv_PigRotateRows[hv_i]);
					(*hv_PigRotateColumnRef)[hv_ColumnRef] = HTuple(hv_PigRotateColumns[hv_i]);
				}
				else
				{
					hv_RightWidthtoCentre[hv_ColumnRef] = HTuple(hv_PigRotateRows[hv_i]);
					(*hv_PigRotateColumnRef)[hv_ColumnRef] = HTuple(hv_PigRotateColumns[hv_i]);
				}
			}
		}
		//
		//*Initialize Pig Width Values
		(*hv_PigWidth) = 0;
		//
		//* Find Width
		//Find Pig Width Array Length
		if (0 != ((hv_RightWidthtoCentre.TupleLength()) > (hv_LeftWidthtoCentre.TupleLength())))
		{
			(*hv_WidthArrayLength) = hv_LeftWidthtoCentre.TupleLength();
		}
		else
		{
			(*hv_WidthArrayLength) = hv_RightWidthtoCentre.TupleLength();
		}
		//
		//* Map Pig Width Profile Points
	{
		HTuple end_val90 = (*hv_WidthArrayLength) - 1;
		HTuple step_val90 = 1;
		for (hv_i = 0; hv_i.Continue(end_val90, step_val90); hv_i += step_val90)
		{
			if (0 != (HTuple(HTuple(hv_RightWidthtoCentre[hv_i]) > 0).TupleAnd(HTuple(hv_LeftWidthtoCentre[hv_i]) > 0)))
			{
				(*hv_PigWidth)[hv_i] = HTuple(hv_LeftWidthtoCentre[hv_i]) - HTuple(hv_RightWidthtoCentre[hv_i]);
			}
		}
	}
	}
	//
	return;
}